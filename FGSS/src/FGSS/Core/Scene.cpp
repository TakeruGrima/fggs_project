#include "Scene.h"

Rect Scene::mGameZone;

Scene::Scene()
{
	mBreakGO = false;
	mGameZone = Rect(0, 0, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
}

void Scene::OnUI()
{
	GUI = new UIPanel("MainPanel", Vector2(0, 0), 
		Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
}

void Scene::LoadContent()
{
	OnUI();
}

void Scene::Update(int elapsedTime)
{
	/*for (GoDictionary::iterator it = Actors.begin(); it != Actors.end(); ++it)
	{
		it->second->Update(elapsedTime);
	}*/

	if (!mBreakGO)
	{
		for (size_t i = 0; i < Actors.size(); i++)
		{
			Actors[i]->Update(elapsedTime);
		}
	}

	GUI->Update(elapsedTime);

	Clean();
}

void Scene::Draw(int elapsedTime)
{
	/*for (GoDictionary::iterator it = Actors.begin(); it != Actors.end(); ++it)
	{
		it->second->Draw(elapsedTime);
	}*/

	SortActorsByDepth();

	for (size_t i = 0; i < Actors.size(); i++)
	{
		Actors[i]->Draw(elapsedTime);
	}

	GUI->Draw(elapsedTime);
}

void Scene::AddActor(GameObject * actor)
{
	/*Actors.insert(std::pair<std::string, GameObject *>(actor->GetName(), actor));*/
	Actors.push_back(actor);
}

GameObject * Scene::GetActor(string name)
{
	//std::vector<GameObject *>::iterator it;
	//
	//it = find(Actors.begin(), Actors.end(), name);
	//if (it != Actors.end())
	//{
	//	return *it;
	//}

	return nullptr;
}

void Scene::SortActorsByDepth()
{
	sort(Actors.begin(), Actors.end(), [](GameObject* go1, GameObject* go2)
	{
		if (go1->GetPosition() == nullptr || go2->GetPosition() == nullptr)
			return false;

		if (go1->GetPosition()->Y + go1->GetBoundingBox()->Height
			< go2->GetPosition()->Y + go2->GetBoundingBox()->Height) return true;
		return false;
	});
}

void Scene::Clean()
{
	auto it = Actors.begin();
	// Iterate through the Actors
	while (it != Actors.end())
	{
		// Check if the actor need to be removed
		GameObject * actor = (*it);
		if (actor->ToRemove)
		{
			it = Actors.erase(it);
			delete actor;
		}
		else
		{
			// Go to next entry in map
			it++;
		}
	}

	//for (size_t i = 0; i < Actors.size(); i++)
	//{
	//	GameObject * actor = Actors[i];
	//	if (actor->ToRemove)
	//	{
	//		delete actor;
	//	}
	//}
}

void Scene::Destroy()
{
	InputManager::Clear();
	ContentManager::Clear();
	/*for (GoDictionary::iterator it = Actors.begin(); it != Actors.end(); ++it)
	{
		it->second->ToRemove = true;
	}*/
	for (size_t i = 0; i < Actors.size(); i++)
	{
		Actors[i]->Destroy();
		delete Actors[i];
	}

	Actors.clear();

	delete GUI;
}

Scene::~Scene()
{
	Destroy();
}
