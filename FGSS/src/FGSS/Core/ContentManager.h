#pragma once
#include <vector>
#include <map>
#include <cassert>

#include "../FGSS.h"
#include "../Core/SpriteFont.h"

typedef std::map < string,Texture2D *> TextureDictionary;
typedef std::map < string, SpriteFont *> SpriteFontDictionary;
typedef std::map < string, SoundEffect *> SoundEffectDictionary;

class ContentManager
{
	static ContentManager * mInstance;

	static string mTexturesRoot;
	static string mSpriteFontsRoot;
	static string mSoundEffectsRoot;
	TextureDictionary mTextures;
	SpriteFontDictionary mSpriteFonts;
	SoundEffectDictionary mSoundEffects;
public:
	ContentManager();

	static ContentManager * Get()
	{
		if (mInstance == nullptr)
			mInstance = new ContentManager();

		return mInstance;
	}

	static void SetTexturesPathRoot(string texturesRoot) { Get()->mTexturesRoot = texturesRoot; }
	static Texture2D * LoadTexture(const char * filePath);
	static SpriteFont * LoadSpriteFont(string path);
	static SoundEffect * LoadSoundEffect(string path);
	static void PlaySoundEffect(string path);
	static void PauseSoundEffect(string path);
	static void StopSoundEffect(string path);

	static void Clear() { delete Get(); }

	~ContentManager();
private:
	// Textures
	static bool IfTextureExist(string textureName);
	static Texture2D * GetTexture(string textureName) { return Get()->mTextures[textureName]; }
	static void AddTexture(Texture2D * texture, string textureName);
	void ClearTextures();

	// SpriteFonts
	static bool IfSpriteFontExist(string name);
	static SpriteFont * GetSpriteFont(string name) { return Get()->mSpriteFonts[name]; }
	static void AddSpriteFont(SpriteFont * spritefont, string name);
	void ClearSpriteFonts();

	// SoundEffects
	static bool IfSoundEffectExist(string name);
	static SoundEffect * GetSoundEffect(string name) { return Get()->mSoundEffects[name]; }
	static void AddSoundEffect(SoundEffect * soundEffect, string name);
	void ClearSoundEffects();
};

