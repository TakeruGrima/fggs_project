#include "InputManager.h"

InputManager * InputManager::mInstance;

InputManager::InputManager()
{

}

#pragma region Key

bool InputManager::GetKeyDown(Input::Keys key)
{
	Input::KeyboardState* prevkeyboardState = &Get()->prevKeyboardState;
	Input::KeyboardState* newkeyboardState = &Get()->newKeyboardState;

	if (newkeyboardState->IsKeyDown(key))
	{
		if (prevkeyboardState == nullptr || !prevkeyboardState->IsKeyDown(key))
			return true;
	}

	return false;
}

bool InputManager::GetKey(Input::Keys key)
{
	Input::KeyboardState* prevkeyboardState = &Get()->prevKeyboardState;
	Input::KeyboardState* newkeyboardState = &Get()->newKeyboardState;

	if (prevkeyboardState != nullptr && newkeyboardState->IsKeyDown(key))
	{
		if (prevkeyboardState->IsKeyDown(key))
			return true;
	}

	return false;
}

bool InputManager::GetKeyUp(Input::Keys key)
{
	Input::KeyboardState* prevkeyboardState = &Get()->prevKeyboardState;
	Input::KeyboardState* newkeyboardState = &Get()->newKeyboardState;

	if (prevkeyboardState != nullptr && newkeyboardState->IsKeyUp(key))
	{
		if (prevkeyboardState->IsKeyDown(key))
			return true;
	}

	return false;
}

#pragma endregion

#pragma region Mouse

bool InputManager::GetMouseButtonDown(eMouseButton button)
{
	Input::MouseState* prevMouseState = &Get()->prevMouseState;
	Input::MouseState* newMouseState = &Get()->newMouseState;

	bool prevIsNull = prevMouseState == nullptr;

	switch (button)
	{
		case InputManager::LEFT:
			if (newMouseState->LeftButton == Input::ButtonState::PRESSED 
				&&(prevIsNull || prevMouseState->LeftButton == Input::ButtonState::RELEASED))
			{
				return true;
			}
			return false;
		case InputManager::MIDDLE:
			if (newMouseState->MiddleButton == Input::ButtonState::PRESSED
				&& (prevIsNull || prevMouseState->MiddleButton == Input::ButtonState::RELEASED))
			{
				return true;
			}
			return false;
		case InputManager::RIGHT:
			if (newMouseState->RightButton == Input::ButtonState::PRESSED
				&& (prevIsNull || prevMouseState->RightButton == Input::ButtonState::RELEASED))
			{
				return true;
			}
			return false;
		default:
			break;
	}

	return false;
}

bool InputManager::GetMouseButton(eMouseButton button)
{
	Input::MouseState* prevMouseState = &Get()->prevMouseState;
	Input::MouseState* newMouseState = &Get()->newMouseState;

	if (prevMouseState == nullptr)return false;

	switch (button)
	{
	case InputManager::LEFT:
		if (newMouseState->LeftButton == Input::ButtonState::PRESSED
			&& (prevMouseState->LeftButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	case InputManager::MIDDLE:
		if (newMouseState->MiddleButton == Input::ButtonState::PRESSED
			&& (prevMouseState->MiddleButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	case InputManager::RIGHT:
		if (newMouseState->RightButton == Input::ButtonState::PRESSED
			&& (prevMouseState->RightButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	default:
		break;
	}

	return false;
}

bool InputManager::GetMouseButtonUp(eMouseButton button)
{
	Input::MouseState* prevMouseState = &Get()->prevMouseState;
	Input::MouseState* newMouseState = &Get()->newMouseState;

	bool prevIsNull = prevMouseState == nullptr;

	switch (button)
	{
	case InputManager::LEFT:
		if (newMouseState->LeftButton == Input::ButtonState::RELEASED
			&& (prevIsNull || prevMouseState->LeftButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	case InputManager::MIDDLE:
		if (newMouseState->MiddleButton == Input::ButtonState::RELEASED
			&& (prevIsNull || prevMouseState->MiddleButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	case InputManager::RIGHT:
		if (newMouseState->RightButton == Input::ButtonState::RELEASED
			&& (prevIsNull || prevMouseState->RightButton == Input::ButtonState::PRESSED))
		{
			return true;
		}
		return false;
	default:
		break;
	}

	return false;
}

#pragma endregion


void InputManager::Update()
{
	InputManager * instance = Get();

	instance->SetPrevState(instance->newKeyboardState, instance->newMouseState);
	instance->SetNewState(*Input::Keyboard::GetState(), *Input::Mouse::GetState());
}

InputManager::~InputManager()
{
	mInstance = nullptr;
}

void InputManager::SetPrevState(Input::KeyboardState & keyboardstate, Input::MouseState & mouseState)
{
	prevKeyboardState = keyboardstate;
	prevMouseState = mouseState;
}

void InputManager::SetNewState(Input::KeyboardState & keyboardstate, Input::MouseState & mouseState)
{
	newKeyboardState = keyboardstate;
	newMouseState = mouseState;
}
