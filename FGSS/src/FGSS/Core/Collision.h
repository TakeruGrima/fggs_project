#pragma once

#include "Sprite.h"

static class Collision
{
public:
	static bool CollideByBox(Sprite *pSprite1,Sprite *pSprite2);
	static bool CollideByBox(Rect *box1,Rect *box2);
};

