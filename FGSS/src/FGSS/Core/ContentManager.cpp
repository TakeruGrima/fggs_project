#include "ContentManager.h"

ContentManager * ContentManager::mInstance = nullptr;
string ContentManager::mTexturesRoot;
string ContentManager::mSpriteFontsRoot;
string ContentManager::mSoundEffectsRoot;

ContentManager::ContentManager()
{
	mTexturesRoot = "Textures/";
	mSpriteFontsRoot = "Fonts/";
	mSoundEffectsRoot = "Sounds/";
}


Texture2D * ContentManager::LoadTexture(const char * filePath)
{
	string stringFilePath(filePath);

	if (IfTextureExist(stringFilePath))
	{
		return Get()->mTextures[stringFilePath];
	}

	Texture2D * texture = new Texture2D();

	string path;
	path.append(Get()->mTexturesRoot);
	path.append(stringFilePath);

	texture->Load(path.c_str(),false);

	AddTexture(texture, stringFilePath);

	return texture;
}

SpriteFont * ContentManager::LoadSpriteFont(string path)
{
	string fullPath = mSpriteFontsRoot + path;

	if (IfSpriteFontExist(fullPath))
	{
		return Get()->mSpriteFonts[fullPath];
	}

	SpriteFont * spriteFont = new SpriteFont(fullPath);

	assert(spriteFont->IsValid());
	AddSpriteFont(spriteFont, fullPath);

	return spriteFont;
}

SoundEffect * ContentManager::LoadSoundEffect(string path)
{
	string fullPath = mSoundEffectsRoot + path;

	SoundEffect * sound = new SoundEffect();
	sound->Load(fullPath.c_str());

	AddSoundEffect(sound, fullPath);

	return sound;
}

void ContentManager::PlaySoundEffect(string path)
{
	string fullPath = mSoundEffectsRoot + path;

	if (IfSoundEffectExist(fullPath))
		Audio::Play(Get()->mSoundEffects[fullPath]);
}

void ContentManager::PauseSoundEffect(string path)
{
	string fullPath = mSoundEffectsRoot + path;

	if (IfSoundEffectExist(fullPath))
		Audio::Pause(Get()->mSoundEffects[fullPath]);
}

void ContentManager::StopSoundEffect(string path)
{
	string fullPath = mSoundEffectsRoot + path;

	if (IfSoundEffectExist(fullPath))
		Audio::Stop(Get()->mSoundEffects[fullPath]);
}

bool ContentManager::IfTextureExist(string textureName)
{
	TextureDictionary textures = Get()->mTextures;

	if (textures.find(textureName) == textures.end()) {
		return false;
	}
	else {
		return true;
	}
}

ContentManager::~ContentManager()
{
	ClearTextures();
	ClearSpriteFonts();
	ClearSoundEffects();

	mInstance = nullptr;
}

void ContentManager::AddTexture(Texture2D * texture,string textureName)
{
	Get()->mTextures.insert(std::pair<std::string, Texture2D *>(textureName, texture));
}

void ContentManager::ClearTextures()
{
	auto it = mTextures.begin();
	// Iterate through the Actors
	while (it != mTextures.end())
	{
		Texture2D * texture = it->second;

		it = mTextures.erase(it);
		delete texture;
	}

	mTextures.clear();
}

bool ContentManager::IfSpriteFontExist(string name)
{
	SpriteFontDictionary spriteFonts = Get()->mSpriteFonts;

	if (spriteFonts.find(name) == spriteFonts.end()) {
		return false;
	}
	else {
		return true;
	}
}

void ContentManager::AddSpriteFont(SpriteFont * spritefont, string name)
{
	Get()->mSpriteFonts.insert(std::pair<std::string, SpriteFont *>(name, spritefont));
}

void ContentManager::ClearSpriteFonts()
{
	auto it = mSpriteFonts.begin();
	// Iterate through the Actors
	while (it != mSpriteFonts.end())
	{
		SpriteFont * spriteFont = it->second;

		it = mSpriteFonts.erase(it);
		delete spriteFont;
	}

	mSpriteFonts.clear();
}

bool ContentManager::IfSoundEffectExist(string name)
{
	SoundEffectDictionary soundEffects = Get()->mSoundEffects;

	if (soundEffects.find(name) == soundEffects.end()) {
		return false;
	}
	else {
		return true;
	}
}

void ContentManager::AddSoundEffect(SoundEffect * soundEffect, string name)
{
	Get()->mSoundEffects.insert(std::pair<std::string, SoundEffect *>(name, soundEffect));
}

void ContentManager::ClearSoundEffects()
{
	auto it = mSoundEffects.begin();
	// Iterate through the Actors
	while (it != mSoundEffects.end())
	{
		SoundEffect * sound = it->second;

		it = mSoundEffects.erase(it);
		delete sound;
	}

	mSpriteFonts.clear();
}
