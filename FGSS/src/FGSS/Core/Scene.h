#pragma once

#include <algorithm>
#include <vector>
#include <map>
#include "ContentManager.h"
#include "InputManager.h"
#include "UI/UIPanel.h"

typedef std::map < std::string, GameObject *> GoDictionary;

using namespace std;

class Scene
{
protected:
	bool mBreakGO;
	UIPanel * GUI;
	/*GoDictionary Actors;*/
	std::vector<GameObject *> Actors;
	static Rect mGameZone;
public:
	Scene();

	/// <summary> All content should be loaded in this method. </summary>
	void virtual LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void virtual Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void virtual Draw(int elapsedTime);

	virtual void Destroy();

	static void SetGameZone(Rect &gameZone) { mGameZone = gameZone; }
	static Rect & GetGameZone() { return mGameZone; }

	~Scene();
protected:
	virtual void OnUI();

	void AddActor(GameObject * actor);
	GameObject * GetActor(string name);

	void SortActorsByDepth();

	void Clean();
};