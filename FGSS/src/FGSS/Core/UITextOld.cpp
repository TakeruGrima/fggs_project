#include "UITextOld.h"
#include "GameObject.h"

UITextOld::UITextOld(string name, Vector2 &pPosition, const Color & pColor) :GameObject(name)
{
	mPosition = new Vector2(pPosition);
	mColor = pColor;
}

UITextOld::UITextOld(string name, char * pText, Vector2 &pPosition, const Color & pColor) :UITextOld(name, pPosition, pColor)
{
	mString = pText;
}

void UITextOld::SetText(char * pText)
{
	mString.clear();
	mString = pText;
}

void UITextOld::SetText(string &pText)
{
	mString = pText;
}

void UITextOld::Draw(int elapsedTime)
{
	if (mIsActive)
	{
		SpriteBatch::DrawString(mString.c_str(), mPosition,
			&mColor);
	}
}


UITextOld::~UITextOld()
{
	if (mPosition != nullptr)
		delete mPosition;
}