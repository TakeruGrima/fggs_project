#include "SpriteFont.h"
#include "ContentManager.h"

SpriteFont::SpriteFont(string xmlPath)
{
	//Load xmlFile
	tinyxml2::XMLDocument doc;
	auto error = doc.LoadFile(xmlPath.c_str());

	if (!error)
	{
		XMLElement * root = doc.FirstChildElement("SpriteFont");

		mCharWidth = root->IntAttribute("charWidth");
		mCharHeight = root->IntAttribute("charHeight");

		XMLElement * element = root->FirstChildElement("Texture");
		mTexture = ContentManager::LoadTexture(element->GetText());

		mCharset = element->NextSiblingElement("Chars")->GetText();
		mCharset.erase(std::remove(mCharset.begin(), mCharset.end(), '\t'), mCharset.end());
		mCharset.erase(std::remove(mCharset.begin(), mCharset.end(), '\n'), mCharset.end());
		mCharset.erase(std::remove(mCharset.begin(), mCharset.end(), -62), mCharset.end());

		mIsValid = true;
	}
	else
		mIsValid = false;
}

void SpriteFont::DrawChar(char c, Vector2 *position, const Color *color)
{
	int id = mCharset.find(c);

	Rect source = Rect(id * mCharWidth, 0, mCharWidth, mCharHeight);

	SpriteBatch::Draw(mTexture, position, &source,Vector2::Zero,1,0,color,SpriteEffect::NONE);
}

int SpriteFont::GetTextWidth(string text)
{
	if(text.find("\n") == -1)
		return text.size() * mCharWidth;

	int width = 0;
	bool newLineFind = true;

	string s = text;
	string delimiter = "\n";

	while (newLineFind)
	{
		int idDelimiter = s.find(delimiter);
		if (idDelimiter != -1)
		{
			string line = s.substr(0, idDelimiter);
			int newWidth = line.size() * mCharWidth;
			if (width < newWidth)
				width = newWidth;
			s = s.substr(idDelimiter+1, s.size() - idDelimiter);
			newLineFind = true;
		}
		else
		{
			newLineFind = false;
		}
	}
	
	return width;
}

int SpriteFont::GetTextHeight(string text)
{
	int occurrences = 0;
	string::size_type start = 0;

	while ((start = text.find('\n', start)) != string::npos) {
		++occurrences;
		start ++;
	}

	return (occurrences+1) * (mCharHeight + mCharHeight/4);
}

void SpriteFont::DrawString(string text, Vector2 * position, const Color *color)
{
	Vector2 currentPosition = *position;

	for (size_t i = 0; i < text.size(); i++)
	{
		char c = text[i];
		if (c == '\n')
		{
			currentPosition.X = position->X;
			currentPosition.Y += mCharHeight + mCharHeight/4;
		}
		else
		{
			DrawChar(c, &currentPosition, color);
			currentPosition.X += mCharWidth;
		}
	}
}

SpriteFont::~SpriteFont()
{
}
