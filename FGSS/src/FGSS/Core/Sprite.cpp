#include "Sprite.h"

Sprite::Sprite(string name) : GameObject(name), FRAME_TIME(0), FRAME_COUNT(1)
{
	mTexture = nullptr;
	mDestRect = nullptr;
	mSourceRect = nullptr;
	mPosition = nullptr;

	mAlpha = 1;

	mColor = *(Color::White);
}

Sprite::Sprite(string name, char * pTexturePath, int frameCount, int pFrameTime) :
	GameObject(name), FRAME_TIME(pFrameTime), FRAME_COUNT(frameCount)
{
	mAlpha = 1;
	mTexture = ContentManager::LoadTexture(pTexturePath);

	mPosition = new Vector2(0, 0);
	mDestRect = nullptr;
	mSourceRect = new Rect(0, 0, mTexture->GetWidth(), mTexture->GetHeight());

	mBoundingBox = new Rect(mPosition->X, mPosition->Y, mTexture->GetWidth(), mTexture->GetHeight());

	mColor = *(Color::White);
}

Sprite::Sprite(string name, char * pTexturePath, Vector2 &pPosition, int frameCount, int pFrameTime) :
	Sprite(name, pTexturePath, frameCount, pFrameTime)
{
	mPosition = new Vector2(pPosition);
}

Sprite::Sprite(string name, char * pTexturePath, Rect &pDestRect, int frameCount, int pFrameTime) :
	Sprite(name, pTexturePath, frameCount, pFrameTime)
{
	mDestRect = new Rect(pDestRect);
}

Sprite::Sprite(string name, char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int frameCount, int pFrameTime) :
	Sprite(name, pTexturePath, pPosition, frameCount, pFrameTime)
{
	mSourceRect = new Rect(pSourceRect);

	mBoundingBox = new Rect(mPosition->X, mPosition->Y, mSourceRect->Width, mSourceRect->Height);
}

Sprite::Sprite(string name, char * pTexturePath, Rect &pDestRect, Rect &pSourceRect, int frameCount, int pFrameTime) :
	Sprite(name, pTexturePath, Vector2(pDestRect.X, pDestRect.Y), pSourceRect, frameCount, pFrameTime)
{

}

void Sprite::UpdateBoundingBox()
{
	mBoundingBox->X = mPosition->X;
	mBoundingBox->Y = mPosition->Y;
}

void Sprite::SetPosition(float x, float y)
{
	mPosition->X = x;
	mPosition->Y = y;

	UpdateBoundingBox();
}

void Sprite::Move(float pX, float pY)
{
	SetPosition(mPosition->X + pX, mPosition->Y + pY);
}

int Sprite::GetWidth()
{
	return mBoundingBox->Width;
}

int Sprite::GetHeight()
{
	return mBoundingBox->Height;
}

void Sprite::TouchBy(GameObject &object)
{
}

void Sprite::Update(int elapsedTime)
{
	if (mIsActive)
	{
		if (mAlpha > 1) mAlpha = 1;
		else if (mAlpha < 0) mAlpha = 0;

		Move(mVx, mVy);

		Animate(elapsedTime);

		mVx = 0;
		mVy = 0;
	}

	UpdateBoundingBox();
}

void Sprite::Draw(int elapsedTime)
{
	if (mIsActive && mTexture != nullptr)
	{
		Color color = mColor;
		color.A = mAlpha;

		if (mDestRect == nullptr)
			SpriteBatch::Draw(mTexture, mPosition, mSourceRect,Vector2::Zero,1,0,&color,SpriteEffect::NONE);
		else
			SpriteBatch::Draw(mTexture, mDestRect, mSourceRect,Vector2::Zero, 1, 0, &color, SpriteEffect::NONE);
	}
	GameObject::Draw(elapsedTime);
}

void Sprite::Destroy()
{
	if (mSourceRect != nullptr)
		delete mSourceRect;
	if (mDestRect != nullptr)
		delete mDestRect;

	mTexture = nullptr;
	mDestRect = nullptr;
	mSourceRect = nullptr;

	GameObject::Destroy();
}

void Sprite::Animate(int elapsedTime)
{
	if (mSourceRect != nullptr && FRAME_TIME > 0)
	{
		mCurrentFrameTime += elapsedTime;

		if (mCurrentFrameTime > FRAME_TIME)
		{
			mFrame++;
			if (mFrame >= FRAME_COUNT)
				mFrame = 0;
			mCurrentFrameTime = 0;
		}

		mSourceRect->X = mSourceRect->Width * mFrame;
	}
}

Sprite::~Sprite()
{
	Destroy();
}