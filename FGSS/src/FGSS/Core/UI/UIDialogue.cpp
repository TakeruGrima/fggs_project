#include "UIDialogue.h"

UIDialogue::UIDialogue(string name, Vector2 & position, string fontPath, string text, char *textureName)
	:UIImage(name, textureName, position)
{
	mUIText = new UIText("Text", Vector2(20,36/8.0f), fontPath, text, Color(255, 255, 255));

	mUIText->SetParent(this);
	mUIText->SetTypingEffect(15);
	mUIText->SetVerticalAlign(UI::CENTERV);
	mUIText->SetNbLine(mNbLine);

	mUIText->FitTextInBox();
	mFullText = mUIText->GetFullText();
	mUIText->WrapText(mFullText);

	mCurrentIndex = 0;
	mTalkKey = Input::Keys::SPACE;
}

void UIDialogue::SetTalkKey(Input::Keys talkKey)
{
	mTalkKey = talkKey;
}

void UIDialogue::Update(int elapsedTime)
{
	if (mIsActive)
	{
		UIImage::Update(elapsedTime);

		if (mUIText->WasDisplayed() && InputManager::GetKeyDown(mTalkKey))
		{
			mCurrentIndex += mUIText->GetDisplayText().size();
			if (mCurrentIndex >= mFullText.size() - 1)
				mIsActive = false;
			else if (mFullText.size() > mCurrentIndex)
			{
				mUIText->WrapText(mFullText.substr(mCurrentIndex));
				mUIText->ResetText();
			}
		}

		mUIText->Update(elapsedTime);
	}
}

void UIDialogue::Draw(int elapsedTime)
{
	if (mIsActive)
	{
		UIImage::Draw(elapsedTime);

		mUIText->Draw(elapsedTime);
	}
}

void UIDialogue::Destroy()
{
	UIImage::Destroy();
	if (mUIText) delete mUIText;
}


UIDialogue::~UIDialogue()
{
	Destroy();
}
