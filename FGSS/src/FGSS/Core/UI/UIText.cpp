#include "UIText.h"

UIText::UIText(string name, Vector2 &position, string fontPath, string text, Color & color
	, int panelW, int panelH) :UIPanel(name, position, panelW, panelH)
{
	mFont = ContentManager::LoadSpriteFont(fontPath);
	mFullText = text;
	mDisplayText = mFullText;
	mColor = color;

	mEffect = NONE;
	mWasDisplayed = false;

	mTextW = mFont->GetTextWidth(mFullText);
	mTextH = mFont->GetTextHeight(mFullText);

	if (panelW == 0)
	{
		if (mTextW > Graphics::GetViewportWidth())
			mTextW = Graphics::GetViewportWidth();
		mBoundingBox->Width = mTextW;
	}
	if (panelH == 0)
	{
		if (mTextH > Graphics::GetViewportHeight())
			mTextH = Graphics::GetViewportHeight();
		mBoundingBox->Height = mTextH;
	}

	FitTextInBox();
	WrapText(mFullText);
}

void UIText::SetTypingEffect(float speedTyping)
{
	SetTextEffect(TYPING);
	mNormalSpeedTyping = speedTyping;
}

void UIText::SetTextEffect(eTextEffect textEffect)
{
	mEffect = textEffect;

	if (mEffect == TYPING)
	{
		ResetText();
	}
}

void UIText::ResetText()
{
	mDisplayText = ""; 
	mCurrentIndex = 0;
	mWasDisplayed = false;
}

void UIText::FitTextInBox()
{
	string line = "";
	string returnString = "";

	istringstream iss(mFullText);
	vector<string> wordArray(std::istream_iterator<std::string>{iss},
		std::istream_iterator<std::string>());

	for (size_t i = 0; i < wordArray.size(); i++)
	{
		string word = wordArray[i];
		if (mFont->GetTextWidth(line + word) > mParent->GetBoundingBox()->Width)
		{
			returnString = returnString + line + '\n';
			line = "";
		}

		line = line + word + ' ';
	}

	mFullText = returnString + line;

	mTextW = mFont->GetTextWidth(mFullText);

	if (mNbLine == -1)
		mTextH = mFont->GetTextHeight(mFullText);
	else
		mTextH = mFont->GetTextHeight("A") * mNbLine;

	mBoundingBox->Width = mTextW;
	mBoundingBox->Height = mTextH;
}

void UIText::WrapText(string text)
{
	if (mNbLine > -1)
	{
		int i = 1;
		int index = 0;

		string subString = text;

		while (i <= mNbLine)
		{
			int id = subString.find('\n');
			if (id != -1)
			{
				index += (id + 1);
				subString = subString.substr(id + 1);
				i++;
			}
			else
			{
				index = text.size();
				i++;
			}
		}

		mFullText = text.substr(0, index);
	}
}

void UIText::Update(int elapsedTime)
{
	UIPanel::Update(elapsedTime);

	FitTextInBox();

	switch (mHAlign)
	{
	case UI::CENTERH:
		mPosition->X += (GetBoundingBox()->Width - mTextW) / 2;
		break;
	default:
		GetBoundingBox()->Width = mTextW;
		break;
	}

	switch (mVAlign)
	{
	case UI::CENTERV:
		mPosition->Y += (GetBoundingBox()->Height - mTextH) / 2;
		break;
	default:
		GetBoundingBox()->Height = mTextH;
		break;
	}

	mBoundingBox = new Rect(mPosition->X, mPosition->Y, mBoundingBox->Width, mBoundingBox->Height);

	mTime += elapsedTime;

	if (mEffect == TYPING && mCurrentIndex < mFullText.size() &&
		mTime >= (60 * (elapsedTime / 100)) / mNormalSpeedTyping)
	{
		mCurrentIndex++;
		mDisplayText = "";
		mDisplayText = mFullText.substr(0, mCurrentIndex);
		mTime = 0;
	}

	if (mEffect == NONE || mCurrentIndex >= mFullText.size())
	{
		mDisplayText = mFullText;
		mWasDisplayed = true;
	}
}

void UIText::Draw(int elapsedTime)
{
	mFont->DrawString(mDisplayText, mPosition, &mColor);

	GameObject::Draw(elapsedTime);
}

void UIText::Destroy()
{
	UIPanel::Destroy();
}

UIText::~UIText()
{
	Destroy();
}
