#include "UIImage.h"

UIImage::UIImage(string name, string texturePath, Vector2 &position)
	:UIPanel(name, position, 0, 0)
{
	mSprite = new Sprite(name, (char*)texturePath.c_str());

	SetBoundingBox(mSprite->GetWidth(), mSprite->GetHeight());
}

UIImage::UIImage(string name,string texturePath, Vector2 &position, Rect & pSourceRect)
	:UIPanel(name, position, 0, 0)
{
	mSprite = new Sprite(name, (char*)texturePath.c_str(), position, pSourceRect,1,0);

	SetBoundingBox(mSprite->GetWidth(), mSprite->GetHeight());
}

UIImage::UIImage(string name, string texturePath, Rect &destination)
	:UIPanel(name, Vector2(destination.X,destination.Y), 0, 0)
{
	mSprite = new Sprite(name, (char*)texturePath.c_str(), destination,1,0);

	SetBoundingBox(destination.Width, destination.Height);
}

UIImage::UIImage(string name, string texturePath, Rect & destination, Rect & pSourceRect)
	:UIPanel(name, Vector2(destination.X, destination.Y), 0, 0)
{
	mSprite = new Sprite(name, (char*)texturePath.c_str(), destination, pSourceRect,1, 0);

	SetBoundingBox(mSprite->GetWidth(), mSprite->GetHeight());
}

void UIImage::Update(int elapsedTime)
{
	UIPanel::Update(elapsedTime);

	mSprite->SetPosition(mPosition->X, mPosition->Y);

	mSprite->Update(elapsedTime);
}

void UIImage::Draw(int elapsedTime)
{
	if (mIsActive)
	{
		mSprite->Draw(elapsedTime);
		UIPanel::Draw(elapsedTime);
	}
}

void UIImage::Destroy()
{
	delete mSprite;
	UIPanel::Destroy();
}

UIImage::~UIImage()
{
	Destroy();
}
