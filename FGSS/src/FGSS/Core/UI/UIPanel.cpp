#include "UIPanel.h"

UIPanel::UIPanel(string name, Vector2 & position, int width, int height)
	:GameObject(name)
{
	mLocalPosition = new Vector2(position);
	SetBoundingBox(width, height);
	mParent = this;

	mHAlign = UI::LEFT;
	mVAlign = UI::TOP;
}

void UIPanel::Update(int elapsedTime)
{
	int x, y;

	x = mLocalPosition->X;
	y = mLocalPosition->Y;

	if (mParent != this)
	{
		x += mParent->GetPosition()->X;
		y += mParent->GetPosition()->Y;
	}

	if (mHAlign == UI::CENTERH)
	{
		x += (mParent->GetWidth() - this->GetWidth()) / 2;
	}
	else if (mHAlign == UI::RIGHT)
	{
		x += mParent->GetWidth() - this->GetWidth();
	}

	if (mVAlign == UI::CENTERV)
	{
		y += (mParent->GetHeight() - this->GetHeight()) / 2;
	}
	else if (mVAlign == UI::BOTTOM)
	{
		y += mParent->GetHeight() - this->GetHeight();
	}

	mPosition->X = x;
	mPosition->Y = y;

	GameObject::Update(elapsedTime);

	UpdateBoundingBox();

	for (size_t i = 0; i < mElements.size(); i++)
	{
		mElements[i]->Update(elapsedTime);
	}
}

void UIPanel::Draw(int elapsedTime)
{
	if (mIsActive)
	{
		GameObject::Draw(elapsedTime);
		for (size_t i = 0; i < mElements.size(); i++)
		{
			mElements[i]->Draw(elapsedTime);
		}
	}
}

void UIPanel::Destroy()
{
	for (size_t i = 0; i < mElements.size(); i++)
	{
		mElements[i]->Destroy();
		delete mElements[i];
	}

	mElements.clear();

	GameObject::Destroy();
}

UIPanel::~UIPanel()
{
	Destroy();
}

void UIPanel::SetBoundingBox(int width, int height)
{
	mBoundingBox = new Rect(mPosition->X, mPosition->Y, width, height);
}

void UIPanel::UpdateBoundingBox()
{
	SetBoundingBox(mBoundingBox->Width, mBoundingBox->Height);
}

void UIPanel::AddUIElement(UIPanel * element)
{
	mElements.push_back(element);
	element->SetParent(this);
}
