#pragma once

#include "UIPanel.h"
#include "../Sprite.h"

class UIImage : public UIPanel
{
private:
	Sprite * mSprite;
public:
	UIImage(std::string name, std::string texturePath, Vector2 &position);
	UIImage(std::string name, std::string texturePath, Vector2 &position, Rect & pSourceRect);
	UIImage(std::string name, std::string texturePath, Rect &pDestRect);
	UIImage(std::string name, std::string texturePath, Rect & destination, Rect & pSourceRect);

	/// <summary> Get SourceRect reference ! WATCH OUT </summary>
	Rect * GetSourceRect() { return mSprite->GetSourceRect(); }

	void SetAlpha(float alpha) { mSprite->SetAlpha(alpha); }
	float GetAlpha() { return mSprite->GetAlpha(); }

	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	void Destroy();

	~UIImage();
};

