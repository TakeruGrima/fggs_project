#pragma once

#include <vector>
#include "../GameObject.h"


namespace UI
{
	// Alignment Vertical
	enum eVAlign { CENTERV, BOTTOM, TOP };
	// Alignment Horizontal
	enum eHAlign { CENTERH, LEFT, RIGHT };
}

class UIPanel : public GameObject
{
protected:
	Vector2 * mLocalPosition;
	UI::eHAlign mHAlign;
	UI::eVAlign mVAlign;

	UIPanel * mParent;
	vector<UIPanel *> mElements;
public:
	UIPanel(string name, Vector2 &position,int width,int height);

	void SetParent(UIPanel * parent) { mParent = parent; }
	void AddUIElement(UIPanel * element);
	void SetHorizontalAlign(UI::eHAlign align) { mHAlign = align; }
	void SetVerticalAlign(UI::eVAlign align) { mVAlign = align; }

	void SetPosition(Vector2 position) { mLocalPosition->X = position.X; mLocalPosition->Y = position.Y; }
	Vector2 & GetLocalPosition() { return *mLocalPosition; }

	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	virtual void Destroy();

	~UIPanel();
protected:
	void SetBoundingBox(int width, int height);
	virtual void UpdateBoundingBox();
};

