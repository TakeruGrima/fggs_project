#pragma once
#include "../InputManager.h"
#include "UIImage.h"
#include "UIText.h"

class UIDialogue : public UIImage
{
private:
	UIText * mUIText;

	Input::Keys mTalkKey;

	string mFullText;
	int mNbLine = 3;
	int mCurrentIndex;
public:
	UIDialogue(string name,Vector2 & position,string fontPath,string text, char *textureName);

	void SetTalkKey(Input::Keys talkKey);

	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	virtual void Destroy();

	~UIDialogue();
};

