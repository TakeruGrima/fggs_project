#pragma once

#include <sstream>

#include "../ContentManager.h"
#include "../SpriteFont.h"
#include "UIPanel.h"

class UIText : public UIPanel
{
	SpriteFont * mFont;
	Color mColor;
	string mFullText;
	string mDisplayText;

	enum eTextEffect { TYPING, NONE };

	eTextEffect mEffect;
	bool mWasDisplayed;

	float mNormalSpeedTyping = 20.0f;
	int mCurrentIndex = 0;
	float mTime = 0;

	int mNbLine = -1;
	int mTextW, mTextH;
public:
	UIText(string name, Vector2 &position,string fontPath, string text,Color & color = Color(255,255,255), int panelW = 0, int panelH = 0);

	void SetTypingEffect(float speedTyping);
	void SetTextEffect(eTextEffect textEffect);
	void SetNbLine(int nbLine) { mNbLine = nbLine; }
	void SetText(string text) { mFullText = text; }
	void ResetText();
	void FitTextInBox();
	void WrapText(string text);

	int GetCurrentIndex() { return mCurrentIndex; }
	string GetDisplayText() { return mDisplayText; }
	string GetFullText() { return mFullText; }
	bool WasDisplayed(){ return mWasDisplayed; }

	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	virtual void Destroy();

	~UIText();
};

