#include "GameObject.h"

GameObject::GameObject(string name, Vector2 & position)
{
	mName = name;
	mPosition = new Vector2(position);
	mBoundingBox = nullptr;
}

void GameObject::Update(int elapsedTime)
{

}

void GameObject::Draw(int elapsedTime)
{
	if (mUseDebug)
	{
		SpriteBatch::DrawRectangle(&Rect(mBoundingBox->X, mBoundingBox->Y + 1, mBoundingBox->Width, 1), Color::Red);
		SpriteBatch::DrawRectangle(
			&Rect(mBoundingBox->X, mBoundingBox->Y + mBoundingBox->Height - 1, mBoundingBox->Width, 1), Color::Red);
		SpriteBatch::DrawRectangle(&Rect(mBoundingBox->X, mBoundingBox->Y + 1, 1, mBoundingBox->Height), Color::Red);
		SpriteBatch::DrawRectangle(&Rect(mBoundingBox->X + mBoundingBox->Width - 1, mBoundingBox->Y + 1, 1, mBoundingBox->Height), Color::Red);
	}
}

string GameObject::GetName()
{
	return mName;
}

void GameObject::SetActive(bool isActive)
{
	mIsActive = isActive;
}

bool GameObject::IsActive()
{
	return mIsActive;
}

void GameObject::Destroy()
{
	if (mPosition != nullptr)
		delete mPosition;
	if (mBoundingBox != nullptr)
		delete mBoundingBox;

	mPosition = nullptr;
	mBoundingBox = nullptr;
}


GameObject::~GameObject()
{
	Destroy();
}
