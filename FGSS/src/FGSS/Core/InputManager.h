#pragma once

#include "../FGSS.h"

class InputManager
{
	static InputManager * mInstance;

	Input::KeyboardState prevKeyboardState;
	Input::MouseState prevMouseState;
	Input::KeyboardState newKeyboardState;
	Input::MouseState  newMouseState;

public:
	enum eMouseButton {LEFT,MIDDLE,RIGHT};
public:
	InputManager();

	static InputManager * Get()
	{
		if (mInstance == nullptr)
			mInstance = new InputManager();

		return mInstance;
	}

	static bool GetKeyDown(Input::Keys key);
	static bool GetKey(Input::Keys key);
	static bool GetKeyUp(Input::Keys key);

	static bool GetMouseButtonDown(eMouseButton button);
	static bool GetMouseButton(eMouseButton button);
	static bool GetMouseButtonUp(eMouseButton button);

	static float GetMouseX() { return Get()->newMouseState.X; }
	static float GetMouseY() { return Get()->newMouseState.Y; }

	static void Clear() { delete Get(); }
	static void Update();

	~InputManager();
private:
	void SetPrevState(Input::KeyboardState &keyboardstate, Input::MouseState &mouseState);
	void SetNewState(Input::KeyboardState &keyboardstate, Input::MouseState &mouseState);
};

