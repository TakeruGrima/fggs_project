#pragma once
#include "../FGSS.h"

#include <sstream>
#include "GameObject.h"

class UITextOld : public GameObject
{
private:
	std::string mString;
public:
	Vector2 * mPosition;
	Color mColor;
public:
	UITextOld(string name, Vector2 &pPosition, const Color & pColor);
	UITextOld(string name, char * pText, Vector2 &pPosition, const Color & pColor);

	void SetText(char * pText);
	void SetText(string &pString);
	void Draw(int elapsedTime);

	~UITextOld();
};