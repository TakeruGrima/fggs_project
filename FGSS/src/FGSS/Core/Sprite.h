#include "../FGSS.h"

#include "GameObject.h"
#include "ContentManager.h"

#pragma once
class Sprite : public GameObject
{
protected:
	Rect * mDestRect;
	Rect* mSourceRect;
	Texture2D* mTexture;
	
	// usually white but can be use for effect
	Color mColor;

	const int FRAME_TIME;
	const int FRAME_COUNT;

	float mAlpha;

	int mFrame;
	int mCurrentFrameTime;

	//velocity
	float mVx;
	float mVy;
protected:
	void virtual Animate(int elapsedTime);
	virtual void UpdateBoundingBox();
public:
	Sprite(string name);
	Sprite(string name, char * pTexturePath, int frameCount = 1, int pFrameTime = 500);
	Sprite(string name, char * pTexturePath, Vector2 &position, int frameCount = 1, int pFrameTime = 500);
	Sprite(string name, char * pTexturePath, Rect &pDestRect, int frameCount = 1, int pFrameTime = 500);
	Sprite(string name, char * pTexturePath, Vector2 &position, Rect &pSourceRect, int frameCount = 1, int pFrameTime = 500);
	Sprite(string name, char * pTexturePath, Rect &pDestRect, Rect &pSourceRect, int frameCount = 1, int pFrameTime = 500);

	/// <summary> Get SourceRect reference ! WATCH OUT </summary>
	Rect * GetSourceRect() { return mSourceRect; }

	void SetPosition(float x, float y);
	void Move(float pX, float pY);

	void SetAlpha(float alpha) { mAlpha = alpha; }
	float GetAlpha() { return mAlpha; }
	void SetColor(Color color) { mColor = color; }
	int GetWidth();
	int GetHeight();

	virtual void TouchBy(GameObject &pObject);

	//Framework methods
	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	virtual void Destroy();

	~Sprite();
};