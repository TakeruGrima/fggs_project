#pragma once

#include "../FGSS.h"

class GameObject
{
public:
	bool ToRemove = false;
protected:
	bool mUseDebug = false;
	string mName;
	Vector2* mPosition;
	Rect * mBoundingBox;
	bool mIsActive = true;
public:
	GameObject(string name, Vector2 & position = Vector2(0,0));

	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	Vector2 * GetPosition() { return mPosition; }
	Rect * GetBoundingBox() { return mBoundingBox; }
	int GetWidth() { return mBoundingBox->Width; }
	int GetHeight() { return mBoundingBox->Height; }

	string GetName();
	void SetActive(bool isActive);
	void SetUseDebug(bool useDebug) { mUseDebug = useDebug; }
	bool IsActive();

	virtual void Destroy();

	~GameObject();
protected:
	virtual void UpdateBoundingBox() {}
};

