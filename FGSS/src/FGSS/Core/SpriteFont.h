#pragma once

#include "../FGSS.h"
#include "../../tinyXml/tinyxml2.h"

using namespace tinyxml2;

class ContentManager;
class SpriteFont
{
	bool mIsValid;

	Texture2D * mTexture;
	int mCharWidth;
	int mCharHeight;

	string mCharset;
public:
	SpriteFont(string xmlPath);

	bool IsValid() { return mIsValid; }

	void DrawChar(char c, Vector2 *position,const Color *color = Color::White);
	int GetTextWidth(string text);
	int GetTextHeight(string text);
	void DrawString(string text, Vector2 * position, const Color *color = Color::White);

	~SpriteFont();
};

