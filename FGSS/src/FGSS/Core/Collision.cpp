#include "Collision.h"

bool Collision::CollideByBox(Sprite *pSprite1, Sprite *pSprite2)
{
	return CollideByBox(pSprite1->GetBoundingBox(), pSprite2->GetBoundingBox());
}

bool Collision::CollideByBox(Rect * box1, Rect * box2)
{
	return box1->Intersects(*box2);
}
