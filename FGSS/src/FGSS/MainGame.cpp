#include "MainGame.h"

#include <sstream>

MainGame::MainGame(int argc, char* argv[]) : Game(argc, argv)
{
	//Initialise important Game aspects
	Audio::Initialise();
	Graphics::Initialise(argc, argv, this, 1024, 768, false, 25, 25, "Pacman & Zelda", 60);
	Input::Initialise();

	// Start the Game Loop - This calls Update and Draw in game loop
	Graphics::StartGameLoop();
}

MainGame::~MainGame()
{
	ScenesManager::DeleteScene();
}

void MainGame::LoadContent()
{
	ScenesManager::LoadScene(ScenesManager::MAIN_TITLE);
}

void MainGame::Update(int elapsedTime)
{
	ScenesManager::Update(elapsedTime);
}

void MainGame::Draw(int elapsedTime)
{
	SpriteBatch::BeginDraw(); // Starts Drawing

	ScenesManager::Draw(elapsedTime);

	SpriteBatch::EndDraw(); // Ends Drawing
}