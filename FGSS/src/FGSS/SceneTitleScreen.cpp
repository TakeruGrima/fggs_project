#include "SceneTitleScreen.h"



SceneTitleScreen::SceneTitleScreen()
{
	mSelectedOption = 0;
	mOptionSelected = false;
	ContentManager::SetTexturesPathRoot("Textures/");
}

void SceneTitleScreen::OnUI()
{
	Scene::OnUI();

	mPacmanTitle = new UIText("PacmanTitle", Vector2(0, 0), 
		"mainFont.xml", "PAC-MAN", Color(255, 255, 255));
	mPacmanTitle->SetHorizontalAlign(UI::CENTERH);
	mPacmanTitle->SetVerticalAlign(UI::TOP);
	mLinkTitle = new UIText("LinkTitle", Vector2(0, 0), "mainFont.xml", 
		"Link to the assessment", Color(255, 255, 255));
	mLinkTitle->SetHorizontalAlign(UI::CENTERH);
	mLinkTitle->SetVerticalAlign(UI::BOTTOM);

	UIPanel * mainPanel = new UIPanel("Main", Vector2(0,0),
		Graphics::GetViewportWidth()/2.2, Graphics::GetViewportHeight() / 4);
	mainPanel->SetHorizontalAlign(UI::CENTERH);
	mainPanel->SetVerticalAlign(UI::CENTERV);

	mCursor = new UIImage("Cursor", "Cursor.png", Vector2(0, 0));
	mCursor->SetPosition(Vector2(-mCursor->GetWidth() *2, 0));
	mCursor->SetActive(false);
	mCursor->SetVerticalAlign(UI::TOP);

	GUI->AddUIElement(mainPanel);
	mainPanel->AddUIElement(mPacmanTitle);
	mainPanel->AddUIElement(mLinkTitle);
	mainPanel->AddUIElement(mCursor);
}

void SceneTitleScreen::LoadContent()
{
	Scene::LoadContent();

	SoundEffect * titleTheme = ContentManager::LoadSoundEffect("Menu Song.wav");

	titleTheme->SetLooping(true);

	ContentManager::PlaySoundEffect("Menu Song.wav");

	// SoundEffects
	ContentManager::LoadSoundEffect("menu tick.wav");
	mSelectEffect = ContentManager::LoadSoundEffect("Menu Selection Click.wav");
}

void SceneTitleScreen::Update(int elapsedTime)
{
	if (mOptionSelected)
	{
		if (mSelectEffect->GetState() == SoundEffectState::STOPPED)
		{
			if (mSelectedOption == 0)
				ScenesManager::LoadScene(ScenesManager::PACMAN_TITLE);
			else
				ScenesManager::LoadScene(ScenesManager::ZELDA_TITLE);

			ScenesManager::Update(elapsedTime);
		}

		return;
	}

	if (InputManager::GetKeyDown(Input::Keys::RETURN) && mCursor->IsActive())
	{
		mOptionSelected = true;
		ContentManager::StopSoundEffect("Menu Song.wav");
		ContentManager::PlaySoundEffect("Menu Selection Click.wav");
	}

	Scene::Update(elapsedTime);

	if(!mCursor->IsActive())
		mCursor->SetActive(true);

	bool selectionChanged = false;

	if (InputManager::GetKeyDown(Input::Keys::UP))
	{
		selectionChanged = true;
		mSelectedOption = abs(mSelectedOption - 1);
	}
	else if (InputManager::GetKeyDown(Input::Keys::DOWN))
	{
		selectionChanged = true;
		mSelectedOption = (mSelectedOption + 1) % 2;
	}

	if (selectionChanged)
	{
		ContentManager::PlaySoundEffect("menu tick.wav");
		if (mSelectedOption == 0)
			mCursor->SetVerticalAlign(UI::TOP);
		else
			mCursor->SetVerticalAlign(UI::BOTTOM);
	}
}

void SceneTitleScreen::Draw(int elapsedTime)
{
	Scene::Draw(elapsedTime);
}

void SceneTitleScreen::Destroy()
{
	Scene::Destroy();
}

SceneTitleScreen::~SceneTitleScreen()
{
	Destroy();
}
