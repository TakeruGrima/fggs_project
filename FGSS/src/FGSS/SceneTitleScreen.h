#pragma once

#include "Core/Scene.h"
#include "Core/UI/UIImage.h"
#include "Core/UI/UIText.h"
#include "ScenesManager.h"

class SceneTitleScreen : public Scene
{
private:
	int mSelectedOption;
	UIImage * mCursor;
	UIText * mPacmanTitle;
	UIText * mLinkTitle;

	SoundEffect * mSelectEffect;

	bool mOptionSelected;
public:
	SceneTitleScreen();

	/// <summary> All content should be loaded in this method. </summary>
	void LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void Draw(int elapsedTime);

	void Destroy();

	~SceneTitleScreen();
protected:
	void OnUI();
};

