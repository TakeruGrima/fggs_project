#pragma once

#include "../FGSS/SceneTitleScreen.h"
#include "../FGSS/Pacman/ScenePacmanGameplay.h"
#include "../FGSS/Pacman/ScenePacmanTitle.h"
#include "../FGSS/Zelda/SceneZeldaGameplay.h"
#include "../FGSS/Zelda/SceneZeldaTitle.h"

class Scene;
class ScenesManager
{
public:
	enum eScene { MAIN_TITLE, PACMAN_TITLE, PACMAN_GAMEPLAY, ZELDA_TITLE, ZELDA_GAMEPLAY };
public:
	static void LoadScene(eScene scene);

	/// <summary> Called every frame - update game logic here. </summary>
	static void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	static void Draw(int elapsedTime);

	static void DeleteScene();
};

