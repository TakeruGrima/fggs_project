#include "Tile.h"


Tile::Tile(string name, Vector2 & position, Texture2D * texture, Rect & sourceRect) :Sprite(name)
{
	mTexture = texture;
	mPosition = new Vector2(position);
	mSourceRect = new Rect(sourceRect.X, sourceRect.Y, sourceRect.Width, sourceRect.Height);
	mBoundingBox = new Rect(position.X, position.Y, sourceRect.Width, sourceRect.Height);
}

Tile::Tile(string name, int col, int lig, int size):Sprite(name)
{
	mBoundingBox = new Rect(col * size, lig * size, size, size);

	mUseDebug = true;
}

Tile::~Tile()
{
}
