#include "Character.h"

Character::Character(string name, char * texturePath, Vector2 &position, Rect &sourceRect, int frameCount, int pFrameTime, float speed):
	Sprite(name, texturePath, position, sourceRect, frameCount, pFrameTime), SPEED(speed)
{
	mLookDir = RIGHT;
	mFrame = 0;
	mCurrentFrameTime = 0;

	SetHitBox();
}

void Character::TouchBy(GameObject * go)
{
}

void Character::Animate(int elapsedTime)
{
	mVelocityX = mVx;
	mVelocityY = mVy;

	mCurrentFrameTime += elapsedTime;

	if (mCurrentFrameTime > FRAME_TIME)
	{
		if (mVx == 0 && mVy == 0)
		{
			mFrame = 0;
		}
		else
		{
			mFrame++;
			if (mFrame >= FRAME_COUNT)
				mFrame = 0;
		}
		mCurrentFrameTime = 0;
	}

	mSourceRect->X = mSourceRect->Width * mFrame;
}

void Character::SetHitBox()
{
	mHitBox = new Rect(*mBoundingBox);
}

void Character::Update(int elapsedTime)
{
	mSourceRect->Y = mSourceRect->Height * mLookDir;

	Sprite::Update(elapsedTime);
	SetHitBox();
}

void Character::Draw(int elapsedTime)
{
	Sprite::Draw(elapsedTime);
}

void Character::Destroy()
{
	if (mHitBox != nullptr) delete mHitBox;
	mHitBox = nullptr;

	Sprite::Destroy();
}

Character::~Character()
{
	Destroy();
}
