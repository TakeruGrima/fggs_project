#include "SceneZeldaTitle.h"

SceneZeldaTitle::SceneZeldaTitle()
{
	ContentManager::SetTexturesPathRoot("Textures/");

	mFadeOut = true;
}

void SceneZeldaTitle::OnUI()
{
	Scene::OnUI();

	mTitleText = new UIImage("Title", "LinkTitleScreenText.png", Vector2(0, 0));
	mTitleText->SetHorizontalAlign(UI::CENTERH);
	mTitleText->SetVerticalAlign(UI::CENTERV);
	mTitleText->SetAlpha(0);

	mAssignment = new UIImage("Assignment", "LinkTitleScreenAssignment.png", Vector2(0, 0));
	mAssignment->SetHorizontalAlign(UI::CENTERH);
	mAssignment->SetVerticalAlign(UI::CENTERV);
	mAssignment->SetAlpha(0);

	mSubtitleText = new UIImage("Subtitle", "LinkTitleScreenSubtitle.png", Vector2(0, 0));
	mSubtitleText->SetHorizontalAlign(UI::CENTERH);
	mSubtitleText->SetVerticalAlign(UI::CENTERV);
	mSubtitleText->SetAlpha(0);

	mRope = new UIImage("rope", "LinkTitleScreenRope.png", Vector2(0, 0));
	mRope->SetHorizontalAlign(UI::CENTERH);
	mRope->SetVerticalAlign(UI::CENTERV);
	mRope->SetAlpha(0);

	mPressEnter = new UIImage("PressEnter", "PressEnter.png", 
		Vector2(0, -Graphics::GetViewportHeight() / 6));
	mPressEnter->SetHorizontalAlign(UI::CENTERH);
	mPressEnter->SetVerticalAlign(UI::BOTTOM);
	mPressEnter->SetAlpha(0);

	GUI->AddUIElement(mRope);
	GUI->AddUIElement(mTitleText);
	GUI->AddUIElement(mAssignment);
	GUI->AddUIElement(mSubtitleText);
	GUI->AddUIElement(mPressEnter);
}

void SceneZeldaTitle::LoadContent()
{
	Scene::LoadContent();

	SoundEffect * theme = ContentManager::LoadSoundEffect("Adventure Theme Intro.wav");

	theme->SetLooping(true);

	ContentManager::PlaySoundEffect("Adventure Theme Intro.wav");
}

void SceneZeldaTitle::Update(int elapsedTime)
{
	float alphaTitle = mTitleText->GetAlpha();
	float alphaSubtitle = mSubtitleText->GetAlpha();
	float alphaAssignment = mAssignment->GetAlpha();
	float alphaPressEnter = mPressEnter->GetAlpha();

	if (alphaTitle > 0.5f)
		mSubtitleText->SetAlpha(alphaSubtitle + 0.005f);
	if (alphaTitle > 0.5f)
	{
		mAssignment->SetAlpha(alphaAssignment + 0.005f);
		mRope->SetAlpha(mRope->GetAlpha() + 0.005f);
	}
	mTitleText->SetAlpha(alphaTitle + 0.005f);

	if (alphaAssignment == 1.0f)
	{
		if (alphaPressEnter == 1.0f)
			mFadeOut = false;
		else if(alphaPressEnter < 0.2f)
			mFadeOut = true;

		if(mFadeOut)
			mPressEnter->SetAlpha(alphaPressEnter + 0.01f);
		else
			mPressEnter->SetAlpha(alphaPressEnter - 0.01f);
	}

	if (alphaTitle > 0.5f && InputManager::GetKeyDown(Input::Keys::RETURN))
	{
		ScenesManager::LoadScene(ScenesManager::ZELDA_GAMEPLAY);
		return;
	}
	Scene::Update(elapsedTime);
}

void SceneZeldaTitle::Draw(int elapsedTime)
{
	Scene::Draw(elapsedTime);
}

void SceneZeldaTitle::Destroy()
{
	Scene::Destroy();
}

SceneZeldaTitle::~SceneZeldaTitle()
{
	Destroy();
}
