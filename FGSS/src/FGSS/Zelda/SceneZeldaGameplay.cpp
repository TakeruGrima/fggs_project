#include "SceneZeldaGameplay.h"

SceneZeldaGameplay::SceneZeldaGameplay()
{
	ContentManager::SetTexturesPathRoot("Textures/");

	mGameOver = false;
}

void SceneZeldaGameplay::OnUI()
{
	Scene::OnUI();

	mLife = new Life(Vector2(64, 32));

	mDialogueBox = new UIDialogue("Dialogue", Vector2(128, Graphics::GetViewportHeight() - (248 + 16))
		,"mainFont.xml",text, "DialogueBox.png");
	mDialogueBox->SetActive(false);

	// Init Game Over panel
	mGameOverPanel = new UIImage("GameOverScreen", "Transparency.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight()));
	mGameOverPanel->SetActive(false);
	UIText * gameOverText = new UIText("GameOverText", Vector2(0, 0), "mainFont.xml", "GAME OVER!", Color(255, 0, 0));
	gameOverText->SetHorizontalAlign(UI::CENTERH);
	gameOverText->SetVerticalAlign(UI::CENTERV);

	GUI->AddUIElement(mGameOverPanel);
	mGameOverPanel->AddUIElement(gameOverText);

	GUI->AddUIElement(mLife);
	GUI->AddUIElement(mDialogueBox);
}

void SceneZeldaGameplay::LoadContent()
{
	Scene::LoadContent();

	mMap = new ZeldaMap("Maps/map.tmx", "");

	mTileSize = mMap->GetTileSize();

	//24 is 48 / 2
	float ballCenter = mTileSize / 2 - 24;

	// Start at the right
	mBallStart = Vector2((ROOM_WIDTH)* mTileSize + ballCenter, (ROOM_HEIGHT * mMap->GetTileSize()) / 2 + ballCenter);
	mBallEnd = Vector2(-mTileSize, mBallStart.Y);

	mPlayer = new Player("link_Walking.png",
		Vector2((ROOM_WIDTH * mMap->GetTileSize()) / 2, (ROOM_HEIGHT * mMap->GetTileSize()) / 2), Rect(0, 0, 16 * 4, 28 * 4), 6, 100, 0.2f);
	mPlayer->SetLives(4);
	mPlayer->SetMaxLives(4);

	mLife->InitLives(mPlayer->GetLivesPointer(), mPlayer->GetMaxLivesPointer());

	AddActor(mMap);
	vector<Tile *> tiles = mMap->GetOverlayTiles();

	for (size_t i = 0; i < tiles.size(); i++)
	{
		AddActor(tiles[i]);
	}
	AddBall();
	AddActor(mPlayer);

	SoundEffect * ambient = ContentManager::LoadSoundEffect("Exploring a cave.wav");

	ambient->SetLooping(true);

	ContentManager::PlaySoundEffect("Exploring a cave.wav");
	ContentManager::LoadSoundEffect("gameover3.wav");
}

void SceneZeldaGameplay::Update(int elapsedTime)
{
	if (mGameOver)
	{
		if (InputManager::GetKeyDown(Input::Keys::RETURN))
		{
			ScenesManager::LoadScene(ScenesManager::MAIN_TITLE);

			ScenesManager::Update(elapsedTime);
		}

		return;
	}
	else
	{
		if (mPlayer->GetState() == Player::DEAD)
		{
			mGameOver = true;

			mGameOverPanel->SetActive(true);

			ContentManager::StopSoundEffect("Exploring a cave.wav");
			ContentManager::PlaySoundEffect("gameover3.wav");
		}

		mPlayer->EnableMovement(!mDialogueBox->IsActive());

		mTimeSinceLastBall += elapsedTime / 100.0f;

		if (mTimeSinceLastBall > BALL_ELAPSEDTIME)
		{
			mTimeSinceLastBall = 0;
			AddBall();
		}

		for (size_t i = 0; i < mBalls.size(); i++)
		{
			Character * ball = mBalls[i];
			ball->Translate(-ball->GetSpeed() * elapsedTime, 0);
		}

		Scene::Update(elapsedTime);

		// Test Tiles collision with player

		TestTilesCollision();

		// Test Balls collision

		TestBallsCollision();
	}
}

void SceneZeldaGameplay::Draw(int elapsedTime)
{
	Scene::Draw(elapsedTime);
}

void SceneZeldaGameplay::Destroy()
{
	Scene::Destroy();
	mBalls.clear();
}

void SceneZeldaGameplay::TestTilesCollision()
{
	vector<Tile*> collideTiles = mMap->CollideWith(mPlayer->GetHitBox());

	for (size_t i = 0; i < collideTiles.size(); i++)
	{
		mPlayer->TouchBy(collideTiles[i]);
	}
}
void SceneZeldaGameplay::TestBallsCollision()
{
	auto it = mBalls.begin();
	// Iterate through the Actors
	while (it != mBalls.end())
	{
		// Check if the actor need to be removed
		Character * ball = (*it);
		if (mPlayer->GetState() == Player::ALIVE &&
			Collision::CollideByBox(ball->GetHitBox(), mPlayer->GetHitBox()))
		{
			mPlayer->TouchBy(ball);
		}
		if (ball != nullptr && *ball->GetPosition() == mBallEnd)
		{
			it = mBalls.erase(it);
			ball->ToRemove = true;
		}
		else
		{
			// Go to next entry in map
			it++;
		}
	}
}

void SceneZeldaGameplay::AddBall()
{
	Character * ball = new Character("Ball", "Ball.png", mBallStart, Rect(0, 0, 12 * 4, 12 * 4), 2, 150, 0.2f);
	mBalls.push_back(ball);
	AddActor(ball);
}

SceneZeldaGameplay::~SceneZeldaGameplay()
{
	Destroy();
}
