#pragma once

#include "../Core/Scene.h"
#include "../Core/Collision.h"
#include "../Core/UI/UIDialogue.h"

#include "Player.h"
#include "Tile.h"
#include "ZeldaMap.h"
#include "UI/Life.h"
#include "../ScenesManager.h"

#define ROOM_WIDTH 16
#define ROOM_HEIGHT 12
#define BALL_ELAPSEDTIME 15

class SceneZeldaGameplay : public Scene
{
private:
	bool mGameOver;

	Player * mPlayer;
	ZeldaMap * mMap;
	vector<Character *> mBalls;

	float mTileSize;
	float mTimeSinceLastBall;

	Vector2 mBallStart;
	Vector2 mBallEnd;

	SpriteFont * mMainFont;

	string text = "";

	// Global UI
	Life * mLife;

	// Game Over UI
	UIImage * mGameOverPanel;

	UIDialogue * mDialogueBox;
public:
	SceneZeldaGameplay();

	/// <summary> All content should be loaded in this method. </summary>
	void LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void Draw(int elapsedTime);


	~SceneZeldaGameplay();
protected:
	void OnUI();

private:
	void Destroy();

	void TestTilesCollision();
	void TestBallsCollision();
	void AddBall();
};

