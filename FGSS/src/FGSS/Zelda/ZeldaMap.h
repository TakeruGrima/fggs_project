#pragma once

#include "../FGSS.h"
#include "../../libtmx_parser/tmxparser.h"
#include "../Core/GameObject.h"
#include "../Core/ContentManager.h"
#include "../Core/Collision.h"

#include "Tile.h"

#include <string>

using namespace tmxparser;
using namespace std;

class ZeldaMap : public GameObject
{
	TmxTileset mTileset;
	Texture2D * mTexture;

	int mNbCol, mNbRow;

	vector<Tile *> mTiles;
	vector<Tile *> mOverlayTiles;

	int **mData;
public:
	ZeldaMap(const std::string &fileName, const std::string &tilesetsPath);

	int GetTileSize() { return mTileset.tileWidth; }
	vector<Tile *> GetOverlayTiles() { return mOverlayTiles; }

	bool CanWalkThrough(int col, int lig);
	vector<Tile *> CollideWith(Rect * hitBox);

	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	~ZeldaMap();
private:
	void LoadTileset(TmxTileset &tileset,const std::string &tilesetsPath);
	void LoadTiles(TmxLayerCollection_t layers);
};

