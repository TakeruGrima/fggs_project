#include "ZeldaMap.h"

ZeldaMap::ZeldaMap(const std::string & fileName, const std::string & tilesetsPath) :GameObject(fileName)
{
	mBoundingBox = new Rect(0, 0, 0, 0);

	tmxparser::TmxReturn error;
	tmxparser::TmxMap map;

	// test from file
	error = tmxparser::parseFromFile(fileName, &map, tilesetsPath);

	if (!error)
	{
		mNbCol = map.width;
		mNbRow = map.height;

		LoadTileset(map.tilesetCollection[0], tilesetsPath);

		mData = new int*[mNbRow];

		for (int i = 0; i < mNbRow; i++)
			mData[i] = new int[mNbCol];

		LoadTiles(map.layerCollection);
	}
}

bool ZeldaMap::CanWalkThrough(int col, int lig)
{
	if (col < 0 || col >= mNbCol || lig < 0 || lig >= mNbRow) return true;
	if (mData[lig][col] == 0)
		return true;
	return false;
}

vector<Tile*> ZeldaMap::CollideWith(Rect * hitBox)
{
	vector<Tile*> collideTiles;

	for (size_t i = 0; i < mTiles.size(); i++)
	{
		Tile * tile = mTiles[i];
		if (Collision::CollideByBox(tile->GetBoundingBox(),hitBox)) 
		{
			int c = tile->GetPosition()->X / mTileset.tileWidth;
			int l = tile->GetPosition()->Y / mTileset.tileHeight;

			if (!CanWalkThrough(c, l))
			{
				collideTiles.push_back(tile);
				return collideTiles;
			}
		}
	}
}

void ZeldaMap::LoadTileset(TmxTileset &tileset, const std::string &tilesetsPath)
{
	mTileset = tileset;

	string globalFilePath = tileset.image.source;

	int i = globalFilePath.find_last_of('/');

	string s = globalFilePath.substr(i, globalFilePath.length() - i);

	string filePath = tilesetsPath;
	filePath.append(s);

	mTexture = ContentManager::LoadTexture(filePath.c_str());
}

void ZeldaMap::LoadTiles(TmxLayerCollection_t layers)
{
	int line;
	int column;

	for (size_t layerId = 0; layerId < layers.size(); layerId++)
	{
		line = 0;
		column = 0;

		TmxLayer layer = layers[layerId];
		TmxLayerTileCollection_t tiles = layer.tiles;
		for (size_t i = 0; i < tiles.size(); i++)
		{
			int gid = tiles[i].gid;
			if (gid > 1)
			{
				float x = column * mTileset.tileWidth;
				float y = line * mTileset.tileHeight;

				Vector2 position = Vector2(x, y);

				int tileFrame = gid - 1;
				int tilesetColumn = tileFrame % mTileset.colCount;
				int tilesetLine = (int)floor((double)tileFrame / (double)mTileset.colCount);

				Rect rect = Rect(tilesetColumn * mTileset.tileWidth + tilesetColumn * mTileset.tileSpacingInImage,
					tilesetLine * mTileset.tileHeight + tilesetLine * mTileset.tileSpacingInImage,
					mTileset.tileWidth, mTileset.tileHeight);

				Tile * tile = new Tile("Tile", position, mTexture, rect);

				if (layer.name == "TilesOverlay")
				{
					mOverlayTiles.push_back(tile);
				}
				else
				{
					mTiles.push_back(tile);
				}
			}
			else
			{
				mData[line][column] = gid;
			}

			column++;
			if (column == mNbCol)
			{
				column = 0;
				line++;
			}
		}
	}
}

void ZeldaMap::Update(int elapsedTime)
{
	GameObject::Update(elapsedTime);
}

void ZeldaMap::Draw(int elapsedTime)
{
	for (size_t i = 0; i < mTiles.size(); i++)
	{
		mTiles[i]->Draw(elapsedTime);
	}
}

ZeldaMap::~ZeldaMap()
{
	for (int i = 0; i < mNbRow; i++)
		delete[] mData[i];
	delete[] mData;
}
