#pragma once
#include <math.h> 
#include "../Core/InputManager.h"

#include "Tile.h"
#include "Character.h"

#define CLAMP(x, upper, lower) (min(upper, max(x, lower)))

class Player : public Character
{
public:
	enum eState { ALIVE,DEAD,HURT};
private:
	float mLives;
	float mMaxLives;
	bool mCanMove;
	eState mState;
	float mTimeInvincible = 5;
	float mCountInvincible = 0;

	float mHurtVelocity = -150;
	float mGroundFriction = 280;
private:
	void HandleInput(int elapsedTime);
protected:
	void SetHitBox();
public:
	Player(char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int frameCount, int pFrameTime,float speed);

	void SetMaxLives(float maxLives) { mMaxLives = maxLives; }
	void SetLives(float lives) { mLives = lives; }
	float * GetLivesPointer() { return &mLives; }
	float * GetMaxLivesPointer() { return &mMaxLives; }

	void TouchBy(GameObject *go);

	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	void EnableMovement(bool canMove);
	eState GetState() { return mState; }
	void Hurt(float amount);

	void Destroy();

	~Player();
};

