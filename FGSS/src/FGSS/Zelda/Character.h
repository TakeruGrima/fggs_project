#pragma once

#include "../Core/Sprite.h"
#include "Tile.h"

class Character : public Sprite
{
public:
	enum eLook {RIGHT = 0,LEFT = 2,UP = 3,DOWN = 1};
protected:
	const float SPEED;
	eLook mLookDir;

	float mVelocityX;
	float mVelocityY;

	Rect * mHitBox;
protected:
	void Animate(int elapsedTime);
	virtual void SetHitBox();
public:
	Character(string name,char * texturePath, Vector2 &position, Rect &sourceRect, int frameCount, int pFrameTime, float speed);

	void Translate(int x, int y) { mVx = x; mVy = y; }

	Rect * GetHitBox() { return mHitBox; }
	float GetSpeed() { return SPEED; }
	float GetVelocityX() { return mVelocityX; }
	float GetVelocityY() { return mVelocityY; }

	void TouchBy(GameObject *go);

	//Framework methods
	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	void Destroy();

	~Character();
};

