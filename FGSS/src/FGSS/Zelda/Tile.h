#pragma once
#include "../Core/Sprite.h"

class Tile : public Sprite
{
public:
	Tile(string name, Vector2 &position, Texture2D * texture, Rect &sourceRect);
	Tile(string name,int col,int lig,int size);
	~Tile();
};

