#include "Player.h"

Player::Player(char * pTexturePath, Vector2 & pPosition, Rect & pSourceRect, int frameCount, int pFrameTime,float speed)
	:Character("Player",pTexturePath,pPosition,pSourceRect,frameCount,pFrameTime,speed)
{
	mState = ALIVE;

	mCanMove = true;

	SoundEffect * hurtSound = ContentManager::LoadSoundEffect("hit.wav");

	hurtSound->SetGain(0.1f);
}

void Player::HandleInput(int elapsedTime)
{
	float velocity = SPEED * elapsedTime;

	// Checks if D key is pressed
	if (InputManager::GetKey(Input::Keys::D))
	{
		mVx = velocity; //Moves Pacman across X axis
		mLookDir = RIGHT;
	}
	// Checks if A key is pressed
	else if (InputManager::GetKey(Input::Keys::A))
	{
		mVx = -velocity; //Moves Pacman across X axis
		mLookDir = LEFT;
	}
	// Checks if W key is pressed
	if (InputManager::GetKey(Input::Keys::W))
	{
		mVy = -velocity; //Moves Pacman across Y axis
		mLookDir = UP;
	}
	// Checks if S key is pressed
	else if (InputManager::GetKey(Input::Keys::S))
	{
		mVy = velocity; //Moves Pacman across Y axis
		mLookDir = DOWN;
	}

	mVelocityX = mVx;
	mVelocityY = mVy;
}

void Player::SetHitBox()
{
	int x, y, width, height;

	width = mBoundingBox->Width / 2;
	height = mBoundingBox->Height / 2.0;	

	x = mPosition->X + width / 2;

	y = mBoundingBox->Bottom() - height/2;

	mHitBox = new Rect(x, y, width, height/2);
}

void Player::TouchBy(GameObject * go)
{
	if (typeid(*go) == typeid(Tile) || typeid(*go) == typeid(Character))
	{
		if (mVelocityX != 0)
			mPosition->X -= mVelocityX;
		if (mVelocityY != 0)
			mPosition->Y -= mVelocityY;

		if (typeid(*go) == typeid(Character))
		{
			Hurt(0.25f);
		}
		else
		{
			mVelocityX = 0;
			mVelocityY = 0;
		}
	}
}

void Player::Update(int elapsedTime)
{
	if(mCanMove)
		HandleInput(elapsedTime);

	if (mState == HURT)
	{
		if (mCountInvincible <= 0)
		{
			mState = ALIVE;
			mAlpha = 1;
		}
		mCountInvincible -= elapsedTime / 100.0f;

		if (!mCanMove)
		{
			float slowDown = mGroundFriction * elapsedTime;

			if (mVelocityX > 0)
				CLAMP(mVelocityX + slowDown, mVelocityX, 0);
			else if (mVelocityX < 0)
				CLAMP(mVelocityX - slowDown, 0, mVelocityX);
			if (mVelocityY > 0)
				CLAMP(mVelocityY + slowDown, mVelocityY, 0);
			else if (mVelocityY < 0)
				CLAMP(mVelocityY - slowDown, 0, mVelocityY);

			mVx = mVelocityX;
			mVy = mVelocityY;

			if (mVx == 0 && mVy)
				EnableMovement(true);
		}
	}

	int screenWidth = Graphics::GetViewportWidth();
	int screenHeight = Graphics::GetViewportHeight();

	if (mVx != 0)
	{
		if (mPosition->X < 0 - mSourceRect->Width / 2)
			mPosition->X = screenWidth - mSourceRect->Width / 2;
		else if (mPosition->X > screenWidth - mSourceRect->Width / 2)
			mPosition->X = 0 - mSourceRect->Width / 2;
	}
	if (mVy != 0)
	{
		if (mPosition->Y < 0 - mSourceRect->Height / 2)
			mPosition->Y = screenHeight - mSourceRect->Height / 2;
		else if (mPosition->Y > screenHeight - mSourceRect->Height / 2)
			mPosition->Y = 0 - mSourceRect->Height / 2;
	}

	Character::Update(elapsedTime);
}

void Player::Draw(int elapsedTime)
{
	Character::Draw(elapsedTime);
}

void Player::EnableMovement(bool canMove)
{
	mCanMove = canMove;
}

void Player::Hurt(float amount)
{
	if (mState != HURT)
	{
		mLives -= amount;

		if (mLives <= 0)
			mState = DEAD;
		else
		{
			ContentManager::PlaySoundEffect("hit.wav");

			mCountInvincible = mTimeInvincible;
			mState = HURT;
			mAlpha = 0.5f;

			mVelocityX *= mHurtVelocity * 100;
			mVelocityY *= mHurtVelocity * 100;
			EnableMovement(false);
		}
	}
}

void Player::Destroy()
{
	Character::Destroy();
}

Player::~Player()
{
	Destroy();
}
