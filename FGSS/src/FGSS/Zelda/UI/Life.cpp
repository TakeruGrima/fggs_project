#include "Life.h"


Life::Life(Vector2 &position):UIPanel("Lives",position,8 * 64,128)
{
	
}

void Life::SetLifeSourceRect(int id,UIImage * life, double lifeDecimalPart)
{
	Rect * source = life->GetSourceRect();

	if (id + lifeDecimalPart == *mPLives)
	{
		int frame = 4 - (lifeDecimalPart * 4);

		source->X = source->Width * frame;
	}
	else if (id > *mPLives - 1)
	{
		source->X = source->Width * 4;
	}
	else
		source->X = source->Width * 0;
}

void Life::InitLives(float * pLives, float * pMaxLives)
{
	mPLives = pLives;
	mPMaxLives = pMaxLives;

	Vector2 pos = *mPosition;

	for (size_t i = 0; i < *mPMaxLives; i++)
	{
		UIImage * life = new UIImage("life" + i, "heart.png", pos, Rect(0, 0, 64, 64));
		AddUIElement(life);
		pos.X += 64;
	}
}

void Life::Update(int elapsedTime)
{
	double integer;
	double decimal = modf(*mPLives, &integer);

	for (size_t i = 0; i < *mPMaxLives; i++)
	{
		SetLifeSourceRect(i, (UIImage *)mElements[i], decimal);
	}
	UIPanel::Update(elapsedTime);
}

void Life::Draw(int elapsedTime)
{
	UIPanel::Draw(elapsedTime);
}

void Life::Destroy()
{
	UIPanel::Destroy();
}

Life::~Life()
{
	Destroy();
}
