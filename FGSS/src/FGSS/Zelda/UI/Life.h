#pragma once

#include "../../Core/UI/UIImage.h"

class Life : public UIPanel
{
private:
	float * mPMaxLives;
	float * mPLives;
public:
	Life(Vector2 &position);

	void InitLives(float *pLives, float *pMaxLives);

	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	void Destroy();

	~Life();
private:
	void SetLifeSourceRect(int id, UIImage * life, double lifeDecimalPart);
};

