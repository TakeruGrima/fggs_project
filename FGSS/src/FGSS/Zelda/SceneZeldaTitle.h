#pragma once

#include "../Core/Scene.h"
#include "../Core/UI/UIImage.h"
#include "../ScenesManager.h"

class SceneZeldaTitle : public Scene
{
private:
	UIImage * mTitleText;
	UIImage * mSubtitleText;
	UIImage * mAssignment;
	UIImage * mRope;
	UIImage * mPressEnter;

	bool mFadeOut;
public:
	SceneZeldaTitle();

	/// <summary> All content should be loaded in this method. </summary>
	void LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void Draw(int elapsedTime);

	void Destroy();

	~SceneZeldaTitle();
protected:
	void OnUI();
};

