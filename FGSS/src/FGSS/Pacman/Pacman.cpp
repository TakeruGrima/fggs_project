#include "Pacman.h"

Pacman::Pacman(char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int pFrameTime) :Sprite("Player", pTexturePath, pPosition, pSourceRect, pFrameTime), SPEED(0.1f)
{
	mDirection = 0;
	mFrame = 0;
	mCurrentFrameTime = 0;
	mState = ALIVE;

	mTimeInvincible = 50;
	mCountInvincible = 0;
}

void Pacman::TouchBy(Sprite &pSprite)
{

}

void Pacman::SetState(eState state)
{
	mState = state;

	if (mState == INVINCIBLE)
	{
		mCountInvincible = mTimeInvincible;
		mAlpha = 0.5f;
	}
}

void Pacman::HandleInput(int elapsedTime)
{
	if (InputManager::GetKey(Input::Keys::LEFTSHIFT))
	{
		mPacmanSpeedMultiplier = 2.0f;
	}
	else
	{
		mPacmanSpeedMultiplier = 1.0f;
	}

	float velocity = SPEED * elapsedTime * mPacmanSpeedMultiplier;

	// Checks if D key is pressed
	if (InputManager::GetKey(Input::Keys::D))
	{
		mVx = velocity; //Moves Pacman across X axis
		mDirection = 0;
	}
	// Checks if A key is pressed
	else if (InputManager::GetKey(Input::Keys::A))
	{
		mVx = -velocity; //Moves Pacman across X axis
		mDirection = 2;
	}
	// Checks if W key is pressed
	if (InputManager::GetKey(Input::Keys::W))
	{
		mVy = -velocity; //Moves Pacman across Y axis
		mDirection = 3;
	}
	// Checks if S key is pressed
	else if (InputManager::GetKey(Input::Keys::S))
	{
		mVy = velocity; //Moves Pacman across Y axis
		mDirection = 1;
	}
}

void Pacman::Animate(int elapsedTime)
{
	mCurrentFrameTime += elapsedTime;

	if (mCurrentFrameTime > FRAME_TIME)
	{
		if (mVx == 0 && mVy == 0)
		{
			mFrame = 0;
		}
		else
		{
			mFrame++;
			if (mFrame >= 2)
				mFrame = 0;
		}
		mCurrentFrameTime = 0;
	}

	mSourceRect->X = mSourceRect->Width * mFrame;
}

void Pacman::Update(int elapsedTime)
{
	if (mState == DEAD) return;

	if (mState == INVINCIBLE)
	{
		if (mCountInvincible <= 0)
		{
			mState = ALIVE;
			mAlpha = 1;
		}
		mCountInvincible -= elapsedTime / 100.0f;
	}

	HandleInput(elapsedTime);

	Rect gameZone = Scene::GetGameZone();

	int screenWidth = gameZone.Width;
	int screenHeight = gameZone.Height;

	mSourceRect->Y = mSourceRect->Height * mDirection;

	if (mVx != 0)
	{
		if (mPosition->X < gameZone.X - mSourceRect->Width / 2)
			mPosition->X = screenWidth - mSourceRect->Width / 2;
		else if (mPosition->X > screenWidth - mSourceRect->Width / 2)
			mPosition->X = gameZone.X - mSourceRect->Width / 2;
	}
	if (mVy != 0)
	{
		if (mPosition->Y < gameZone.Y - mSourceRect->Height / 2)
			mPosition->Y = screenHeight - mSourceRect->Height / 2;
		else if (mPosition->Y > screenHeight - mSourceRect->Height / 2)
			mPosition->Y = gameZone.Y - mSourceRect->Height / 2;
	}

	Sprite::Update(elapsedTime);
}

void Pacman::Draw(int elapsedTime)
{
	if (mState == DEAD) return;

	Sprite::Draw(elapsedTime);
}

void Pacman::Destroy()
{
	Sprite::Destroy();
}

Pacman::~Pacman()
{
	Destroy();
}