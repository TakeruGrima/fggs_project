#pragma once

#include <cstdlib>
#include <time.h>

#include "../Core/Scene.h"
#include "../Core/Sprite.h"
#include "../Core/Collision.h"
#include "../ScenesManager.h"
#include "../Core/SpriteFont.h"
#include "../Core/UI/UIImage.h"
#include "../Core/UI/UIText.h"
#include "Pacman.h"
#include "Enemy.h"
#include "MovingEnemy.h"

#define MUNCHIECOUNT 50
#define GHOSTCOUNT 10

class ScenePacmanGameplay : public Scene
{
	SpriteFont * mMainFont;

	Pacman * mPlayer;
	int mCollected;
	bool mPlayerIsInvincible;
	bool mWin;
	int topUIHeight;

	// Munchie
	vector<Enemy *> mMunchies;

	// Ghosts
	MovingEnemy * mGhosts[GHOSTCOUNT];

	// Cherry
	Sprite * mCherry;

	// Global UI
	UIImage * mMunchieUI;
	UIText * mMunchieCountUI;

	// Pause screen
	UIImage * mPauseScreen;

	// GameOver screen
	UIImage * mGameOverScreen;

	// Win screen
	UIImage * mWinScreen;

	bool mPrevKeyDown;
	bool mPaused;
	bool mGameOver;
public:
	ScenePacmanGameplay();

	/// <summary> All content should be loaded in this method. </summary>
	void LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void Draw(int elapsedTime);

	void Destroy();

	~ScenePacmanGameplay();
private:

	// Munchies Methods
	void LoadMunchies();
	void ResetMunchiePosition();
	void CheckMunchiesCollision();

	// Cherry Methods
	void LoadCherry();
	void SpawnCherry();
	void CheckCherryCollision();

	// Ghosts Methods
	void LoadGhosts();
	void CheckGhostCollisions();
protected:
	void OnUI();
};

