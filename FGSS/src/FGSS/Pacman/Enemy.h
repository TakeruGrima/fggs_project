#include "../Core/Sprite.h"

class Enemy : public Sprite
{
protected:
	//void Animate(int elapsedTime);
public:
	Enemy(string name,char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int pFrameTime,int startFrame = 0);

	void TouchBy(Sprite &pSprite);

	//Framework methods
	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	void Destroy();

	~Enemy();
};

