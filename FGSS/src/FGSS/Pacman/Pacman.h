#pragma once

#include "../Core/Sprite.h"
#include "../Core/InputManager.h"
#include "../Core/Scene.h"

class Pacman : public Sprite
{
public:
	enum eState { ALIVE, DEAD,INVINCIBLE };
private:
	const float SPEED;
	float mPacmanSpeedMultiplier;

	int mDirection;
	eState mState;

	float mTimeInvincible;
	float mCountInvincible;
public:
	Pacman(char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int pFrameTime);

	void TouchBy(Sprite &pSprite);

	void SetState(eState state);
	eState GetState() { return mState; }

	//Framework methods
	void Update(int elapsedTime);
	void Draw(int elapsedTime);

	void Destroy();

	~Pacman();
protected:
	void Animate(int elapsedTime);
private:
	void HandleInput(int elapsedTime);
};