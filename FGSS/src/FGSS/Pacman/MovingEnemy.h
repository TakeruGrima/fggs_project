#pragma once
#include <cstdlib>
#include <time.h>
#include <iostream>

#include "../FGSS.h"
#include "../Core/Scene.h"
#include "../Core/Sprite.h"

class MovingEnemy : public Sprite
{
public:
	enum eIAMode { RANDOM, PATROL, CHASING, NONE };
private:
	int mDirection;
	float mSpeed;

	float mVelocity;
	float mProgressLerp;

	eIAMode mMode = eIAMode::PATROL;
	eIAMode mDefaultMode;

	int mCumulTime;
	Vector2 mLastPosition;
	Vector2 mNextPosition;

	Sprite * mTargetSprite;
	Rect mGameZone;
public:
	MovingEnemy(string name, char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int frameCount, int pFrameTime, float speed);

	void TouchBy(Sprite &pSprite);

	// IA Methods
	void SetIAMode(eIAMode mode);
	void ResetIAMode() { mMode = mDefaultMode; }

	void SetTarget(Sprite * pTargetSprite);

	eIAMode GetIAMode() { return mMode; }

	//Framework methods
	virtual void Update(int elapsedTime);
	virtual void Draw(int elapsedTime);

	void Destroy();

	~MovingEnemy();
private:
	void ExecuteRandomMode();
	void ExecutePatrolMode();
	void ExecuteChasingMode();

	void MoveTo(Vector2 *position);
};

