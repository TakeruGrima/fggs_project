#include "Enemy.h"

Enemy::Enemy(string name,char * pTexturePath, Vector2 & pPosition, Rect & pSourceRect, int pFrameTime, int startFrame):Sprite(name, pTexturePath, pPosition, pSourceRect, pFrameTime)
{
	mFrame = startFrame;
}

void Enemy::TouchBy(Sprite & pSprite)
{
}

void Enemy::Update(int elapsedTime)
{
	Sprite::Update(elapsedTime);
}

void Enemy::Draw(int elapsedTime)
{
	Sprite::Draw(elapsedTime);
}

void Enemy::Destroy()
{
	Sprite::Destroy();
}

Enemy::~Enemy()
{
	Destroy();
}
