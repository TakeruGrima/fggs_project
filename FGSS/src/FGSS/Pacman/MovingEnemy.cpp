#include "MovingEnemy.h"

#define PI 3.14159265

MovingEnemy::MovingEnemy(string name, char * pTexturePath, Vector2 &pPosition, Rect &pSourceRect, int frameCount, int pFrameTime, float speed) :
	Sprite(name, pTexturePath, pPosition, pSourceRect, frameCount, pFrameTime)
{
	mSpeed = speed;
	mCumulTime = 0;
	mNextPosition = *mPosition;

	mGameZone = Scene::GetGameZone();

	int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
	int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;

	mNextPosition = Vector2(x, y);

	mDefaultMode = NONE;
}

void MovingEnemy::TouchBy(Sprite & pSprite)
{
}

void MovingEnemy::SetIAMode(eIAMode mode)
{
	if (mDefaultMode == NONE)
		mDefaultMode = mode;

	mMode = mode;
}

void MovingEnemy::ExecuteRandomMode()
{
	if (Vector2::Distance(*mPosition, mNextPosition) <= 1)
	{
		int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
		int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;

		mNextPosition = Vector2(x,y);
	}
	else
		MoveTo(&mNextPosition);
}

void MovingEnemy::ExecutePatrolMode()
{
	if (mDirection == 0 && mPosition->X + mSourceRect->Width >=mGameZone.Width) //Hits Right edge
	{
		mDirection = 1; //Change direction
	}
	else if (mDirection == 2 && mPosition->X <= mGameZone.X) //Hits left edge
	{
		mDirection = 3; //Change direction
	}
	else if (mDirection == 1 && mPosition->Y + mSourceRect->Height >= mGameZone.Height) //Hits Down edge
	{
		mDirection = 2; //Change direction
	}
	else if (mDirection == 3 && mPosition->Y <= mGameZone.Y) //Hits up edge
	{
		mDirection = 0; //Change direction
	}
}

void MovingEnemy::ExecuteChasingMode()
{
	MoveTo(mTargetSprite->GetPosition());
}

void MovingEnemy::MoveTo(Vector2 * position)
{
	Vector2 direction = Vector2(*mPosition - *position);

	direction.Normalize();

	mPosition->X = mPosition->X - (direction.X * mVelocity);
	mPosition->Y = mPosition->Y - (direction.Y* mVelocity);
}

void MovingEnemy::SetTarget(Sprite * pTargetSprite)
{
	mTargetSprite = pTargetSprite;
}

void MovingEnemy::Update(int elapsedTime)
{
	mCumulTime += elapsedTime;

	mVelocity = mSpeed * elapsedTime;

	mProgressLerp += mSpeed * elapsedTime;

	if (mMode == eIAMode::PATROL)
	{
		ExecutePatrolMode();

		if (mDirection == 0) //Moves Right
		{
			mPosition->X += mVelocity;
		}
		else if (mDirection == 2) //Moves Left
		{
			mPosition->X -= mVelocity;
		}
		else if (mDirection == 3) //Moves Up
		{
			mPosition->Y -= mVelocity;
		}
		else if (mDirection == 1) //Moves Down
		{
			mPosition->Y += mVelocity;
		}
	}
	else if (mMode == eIAMode::CHASING)
	{
		ExecuteChasingMode();
	}
	else if (mMode == eIAMode::RANDOM)
	{
		ExecuteRandomMode();
	}

	mSourceRect->Y = mSourceRect->Height * mDirection;

	Sprite::Update(elapsedTime);
}

void MovingEnemy::Draw(int elapsedTime)
{
	Sprite::Draw(elapsedTime);
}

void MovingEnemy::Destroy()
{
	Sprite::Destroy();
}


MovingEnemy::~MovingEnemy()
{
	Destroy();
}