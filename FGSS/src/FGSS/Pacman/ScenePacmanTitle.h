#pragma once
#include "../ScenesManager.h"
#include "../Core/Scene.h"
#include "../Core/Sprite.h"
#include "../Core/UITextOld.h"
#include "../Core/SpriteFont.h"

class ScenePacmanTitle : public Scene
{
private:
	SpriteFont * mMainFont;
public:
	ScenePacmanTitle();

	/// <summary> All content should be loaded in this method. </summary>
	void LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void Draw(int elapsedTime);

	void Destroy();

	~ScenePacmanTitle();
protected:
	void OnUI();
};

