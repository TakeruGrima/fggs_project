#include "ScenePacmanTitle.h"

ScenePacmanTitle::ScenePacmanTitle()
{
	ContentManager::SetTexturesPathRoot("Textures/");
}

void ScenePacmanTitle::OnUI()
{
	Scene::OnUI();

	UIImage * background = new UIImage("StartScreen", "Transparency.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(),Graphics::GetViewportHeight()));

	UIText * title = new UIText("Title", Vector2(0,0), "mainFont.xml", "PAC-MAN", Color(255,216, 0));
	title->SetHorizontalAlign(UI::CENTERH);
	title->SetVerticalAlign(UI::CENTERV);

	UIText * startText = new UIText("PressSpace", Vector2(0, -50), "mainFont.xml", "Press Space!", Color(255, 0, 0));
	startText->SetHorizontalAlign(UI::CENTERH);
	startText->SetVerticalAlign(UI::BOTTOM);

	GUI->AddUIElement(background);
	GUI->AddUIElement(startText);
	GUI->AddUIElement(title);
}

void ScenePacmanTitle::LoadContent()
{
	Scene::LoadContent();

	SoundEffect * titleTheme = ContentManager::LoadSoundEffect("Menu Song.wav");

	titleTheme->SetLooping(true);

	ContentManager::PlaySoundEffect("Menu Song.wav");
}

void ScenePacmanTitle::Update(int elapsedTime)
{
	if (InputManager::GetKeyDown(Input::Keys::SPACE))
	{
		ScenesManager::LoadScene(ScenesManager::PACMAN_GAMEPLAY);
		return;
	}

	Scene::Update(elapsedTime);
}


void ScenePacmanTitle::Draw(int elapsedTime)
{
	Scene::Draw(elapsedTime);
}

void ScenePacmanTitle::Destroy()
{
	Scene::Destroy();
}

ScenePacmanTitle::~ScenePacmanTitle()
{
	Destroy();
}
