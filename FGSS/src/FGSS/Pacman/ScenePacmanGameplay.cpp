#include "ScenePacmanGameplay.h"

ScenePacmanGameplay::ScenePacmanGameplay() :Scene()
{
	mPrevKeyDown = false;
	mPaused = false;
	mGameOver = false;
	mCollected = 0;
	mPlayerIsInvincible = false;
	mWin = false;
	topUIHeight = 40;

	ContentManager::SetTexturesPathRoot("Textures/");

	Scene::SetGameZone(Rect(0, topUIHeight, Graphics::GetViewportWidth(),
		Graphics::GetViewportHeight() - topUIHeight));
}

void ScenePacmanGameplay::OnUI()
{
	Scene::OnUI();

	// Global UI

	mMunchieUI = new UIImage("MunchieUI", "UIBackground.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), topUIHeight));
	mMunchieCountUI = new UIText("MunchieCount", Vector2(20, 0), "mainFont.xml", "0", Color(255, 255, 255));

	UIImage * munchieImage = new UIImage("MunchieImage", "MunchieUI.png", Vector2(0, 10));

	GUI->AddUIElement(mMunchieUI);
	mMunchieUI->AddUIElement(munchieImage);
	mMunchieUI->AddUIElement(mMunchieCountUI);

	// Pause Screen
	mPauseScreen = new UIImage("PauseScreen", "Transparency.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight()));
	mPauseScreen->SetActive(false);

	UIText * pauseText = new UIText("PauseText", Vector2(0, 0), "mainFont.xml", "Pause", Color(255, 0, 0));
	pauseText->SetHorizontalAlign(UI::CENTERH);
	pauseText->SetVerticalAlign(UI::CENTERV);

	GUI->AddUIElement(mPauseScreen);
	mPauseScreen->AddUIElement(pauseText);

	// GameOver Screen
	mGameOverScreen = new UIImage("GameOverScreen", "Transparency.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight()));
	mGameOverScreen->SetActive(false);
	UIText * gameOverText = new UIText("GameOverText", Vector2(0, 0), "mainFont.xml", "GAME OVER!", Color(255, 0, 0));
	gameOverText->SetHorizontalAlign(UI::CENTERH);
	gameOverText->SetVerticalAlign(UI::CENTERV);

	GUI->AddUIElement(mGameOverScreen);
	mGameOverScreen->AddUIElement(gameOverText);

	// Win Screen

	mWinScreen = new UIImage("WinScreen", "Transparency.png", Vector2(0, 0),
		Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight()));
	mWinScreen->SetActive(false);
	UIText * winText = new UIText("WinText", Vector2(0, 0), "mainFont.xml", "VICTORY !", Color(255, 0, 0));
	winText->SetHorizontalAlign(UI::CENTERH);
	winText->SetVerticalAlign(UI::CENTERV);

	GUI->AddUIElement(mWinScreen);
	mWinScreen->AddUIElement(winText);
}

void ScenePacmanGameplay::LoadContent()
{
	mMainFont = new SpriteFont("Fonts/mainFont.xml");
	Scene::LoadContent();

	// Load Pacman
	mPlayer = new Pacman("Pacman.tga", Vector2(350.0f, 350.0f), Rect(0.0f, 0.0f, 32, 32), 250);

	AddActor(mPlayer);

	// Load Munchies
	LoadMunchies();

	// Load Cherry

	LoadCherry();

	// Load Ghosts
	LoadGhosts();

	//Load SoundEffects
	ContentManager::LoadSoundEffect("pop.wav");
	ContentManager::LoadSoundEffect("GameOver_0.wav");
	ContentManager::LoadSoundEffect("Victory!.wav");

	SoundEffect * ambiant = ContentManager::LoadSoundEffect("12barsblues_practice.wav");

	ambiant->SetLooping(true);

	SoundEffect * invincible = ContentManager::LoadSoundEffect("PoweringUp.wav");

	invincible->SetLooping(true);

	ContentManager::PlaySoundEffect("12barsblues_practice.wav");
}

void ScenePacmanGameplay::Update(int elapsedTime)
{
	if (mCollected == MUNCHIECOUNT)
	{
		mWin = true;
		mWinScreen->SetActive(true);

		ContentManager::StopSoundEffect("12barsblues_practice.wav");
		ContentManager::PlaySoundEffect("Victory!.wav");
	}
	else if (mPlayer->GetState() == Pacman::DEAD && !mGameOver)
	{
		mGameOver = true;
		mGameOverScreen->SetActive(true);

		ContentManager::StopSoundEffect("12barsblues_practice.wav");
		ContentManager::PlaySoundEffect("GameOver_0.wav");
	}

	if ((mWin || mGameOver) && InputManager::GetKeyDown(Input::Keys::RETURN))
	{
		ScenesManager::LoadScene(ScenesManager::MAIN_TITLE);
		ScenesManager::Update(elapsedTime);
		return;
	}

	if (!mPaused && !mGameOver && !mWin)
	{
		Scene::Update(elapsedTime);

		CheckMunchiesCollision();

		CheckCherryCollision();

		if (mPlayerIsInvincible && mPlayer->GetState() == Pacman::ALIVE)
		{
			mPlayerIsInvincible = false;
			SpawnCherry();
			ContentManager::PlaySoundEffect("12barsblues_practice.wav");
			ContentManager::StopSoundEffect("PoweringUp.wav");
		}

		ResetMunchiePosition();

		CheckGhostCollisions();

		for (size_t i = 0; i < GHOSTCOUNT; i++)
		{
			int distance = Vector2::Distance(*mPlayer->GetPosition(), *mGhosts[i]->GetPosition());

			if (distance < 150)
				mGhosts[i]->SetIAMode(MovingEnemy::CHASING);
			else
				mGhosts[i]->ResetIAMode();
		}

		// move first munchie
		if (InputManager::GetMouseButton(InputManager::eMouseButton::LEFT))
		{
			Enemy * munchie = mMunchies[0];
			munchie->Move(InputManager::GetMouseX() - munchie->GetPosition()->X, InputManager::GetMouseY() - munchie->GetPosition()->Y);
		}
	}

	if (InputManager::GetKeyDown(Input::Keys::P) && !mGameOver && !mWin)
	{
		mPaused = !mPaused;

		mPauseScreen->SetActive(mPaused);
	}
}

void ScenePacmanGameplay::Draw(int elapsedTime)
{
	Scene::Draw(elapsedTime);
}

void ScenePacmanGameplay::Destroy()
{
	mMunchies.clear();
	Scene::Destroy();
}

void ScenePacmanGameplay::LoadMunchies()
{
	for (size_t i = 0; i < MUNCHIECOUNT; i++)
	{
		int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
		int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;
		;

		std::stringstream stream;

		stream << "Munchie_" << i;

		mMunchies.push_back(new Enemy(stream.str(), "Munchie.png", Vector2(x, y), Rect(0, 0, 12, 12),
			rand() % 500 + 50, rand() % 1));
		AddActor(mMunchies[i]);
	}
}

void ScenePacmanGameplay::ResetMunchiePosition()
{
	if (InputManager::GetKeyDown(Input::Keys::R))
	{
		for (size_t i = 0; i < MUNCHIECOUNT; i++)
		{
			int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
			int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;

			Vector2 * position = mMunchies[i]->GetPosition();

			mMunchies[i]->Move(x - position->X, y - position->Y);
		}
	}
}

void ScenePacmanGameplay::CheckMunchiesCollision()
{
	int idMunchieToRemove = -1;
	for (size_t i = 0; i < mMunchies.size(); i++)
	{
		if (Collision::CollideByBox(mPlayer, mMunchies[i]))
		{
			// Remove munchie
			idMunchieToRemove = i;
			mMunchies[i]->ToRemove = true;

			// Collect munchie
			mCollected++;
			ContentManager::PlaySoundEffect("pop.wav");

			std::stringstream stream;

			stream << mCollected;

			mMunchieCountUI->SetText(stream.str());
		}
	}

	if (idMunchieToRemove > -1)
		mMunchies.erase(mMunchies.begin() + idMunchieToRemove);
}

void ScenePacmanGameplay::LoadCherry()
{
	mCherry = new Sprite("Cherry", "Cherry.png", Vector2(0,0), Rect(0, 0, 32, 32), 350);

	AddActor(mCherry);

	SpawnCherry();
}

void ScenePacmanGameplay::SpawnCherry()
{
	int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
	int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;

	mCherry->SetPosition(x, y);
	mCherry->SetActive(true);
}

void ScenePacmanGameplay::CheckCherryCollision()
{
	if (mCherry->IsActive() && Collision::CollideByBox(mPlayer,mCherry))
	{
		mPlayer->SetState(Pacman::INVINCIBLE);
		mPlayerIsInvincible = true;
		mCherry->SetActive(false);

		ContentManager::PauseSoundEffect("12barsblues_practice.wav");
		ContentManager::PlaySoundEffect("PoweringUp.wav");
	}
}

void ScenePacmanGameplay::LoadGhosts()
{
	for (size_t i = 0; i < GHOSTCOUNT; i++)
	{
		std::stringstream stream;

		int x = rand() % Scene::GetGameZone().Width + Scene::GetGameZone().X;
		int y = rand() % Scene::GetGameZone().Height + Scene::GetGameZone().Y;

		stream << "Ghost" << i;
		mGhosts[i] = new MovingEnemy(stream.str(), "GhostBlue.png",
			Vector2(x, y), Rect(0.0f, 0.0f, 24, 24), 0, 0, 0.05f);

		mGhosts[i]->SetIAMode((MovingEnemy::eIAMode)(rand() % 2));
		mGhosts[i]->SetTarget(mPlayer);

		AddActor(mGhosts[i]);
	}
}

void ScenePacmanGameplay::CheckGhostCollisions()
{
	Pacman::eState playerState = mPlayer->GetState();

	if (playerState == Pacman::ALIVE)
	{
		for (size_t i = 0; i < GHOSTCOUNT; i++)
		{
			if (Collision::CollideByBox(mPlayer, mGhosts[i]) )
			{
				mPlayer->SetState(Pacman::DEAD);
				
				return;
			}
		}
	}
}

ScenePacmanGameplay::~ScenePacmanGameplay()
{
	Destroy();
}
