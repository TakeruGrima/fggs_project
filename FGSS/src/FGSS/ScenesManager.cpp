#include "ScenesManager.h"

static Scene * mCurrentScene;

void ScenesManager::LoadScene(eScene scene)
{
	DeleteScene();

	switch (scene)
	{
	case ScenesManager::MAIN_TITLE:
		mCurrentScene = new SceneTitleScreen();
		break;
	case ScenesManager::PACMAN_TITLE:
		mCurrentScene = new ScenePacmanTitle();
		break;
	case ScenesManager::PACMAN_GAMEPLAY:
		mCurrentScene = new ScenePacmanGameplay();
		break;
	case ScenesManager::ZELDA_TITLE:
		mCurrentScene = new SceneZeldaTitle();
		break;
	case ScenesManager::ZELDA_GAMEPLAY:
		mCurrentScene = new SceneZeldaGameplay();
		break;
	default:
		break;
	}

	if(mCurrentScene != nullptr)
		mCurrentScene->LoadContent();
}

void ScenesManager::Update(int elapsedTime)
{
	InputManager::Update();
	if (mCurrentScene != nullptr)
		mCurrentScene->Update(elapsedTime);
}

void ScenesManager::Draw(int elapsedTime)
{
	if (mCurrentScene != nullptr)
		mCurrentScene->Draw(elapsedTime);
}

void ScenesManager::DeleteScene()
{
	InputManager::Clear();
	if (mCurrentScene != nullptr)
	{
		mCurrentScene->Destroy();
		//delete mCurrentScene;
	}
}
